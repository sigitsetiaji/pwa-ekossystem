'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "version.json": "a9811a63b8757808ef337f8a0fee6c61",
"favicon.ico": "f638c58e226bd787846952880e480678",
"index.html": "70b7020256952dc5519819b2b0113702",
"/": "70b7020256952dc5519819b2b0113702",
"main.dart.js": "83e5edf6f50927f11a7aaceb7ef5cb3c",
"flutter.js": "0816e65a103ba8ba51b174eeeeb2cb67",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"icons/favicon-16x16.png": "d0ef0777648fde17c1651f2e0d603000",
"icons/favicon.ico": "f638c58e226bd787846952880e480678",
"icons/apple-icon.png": "201dd6581c6bfc6a266d93d36135f152",
"icons/apple-icon-144x144.png": "fc1ca58ada975aca847c85e579f2fa3f",
"icons/android-icon-192x192.png": "d3e73c24f69da74f3fca48df6c9f4b9c",
"icons/apple-icon-precomposed.png": "201dd6581c6bfc6a266d93d36135f152",
"icons/apple-icon-114x114.png": "416ec5e4c4d01ed9738f2df6d988ae53",
"icons/ms-icon-310x310.png": "3e7c5f5e28ddaf5c091143dc2d9cb49b",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"icons/ms-icon-144x144.png": "fc1ca58ada975aca847c85e579f2fa3f",
"icons/apple-icon-57x57.png": "63f303d512249c5d9f23a5c10f8cd1d3",
"icons/apple-icon-152x152.png": "69600948165cdc2bd1e595aae665d6f6",
"icons/ms-icon-150x150.png": "c314148e50db8e647798ebc393233d69",
"icons/android-icon-72x72.png": "3b7d20a16dcb1bc82279ef16bbe4a5f3",
"icons/android-icon-96x96.png": "51ff721b7bdb38de99beab0bd493aecb",
"icons/android-icon-36x36.png": "aaff26331bd53674930a151f3fcd7081",
"icons/apple-icon-180x180.png": "b82b5828d7590112e917cfea77bd1e75",
"icons/favicon-96x96.png": "51ff721b7bdb38de99beab0bd493aecb",
"icons/android-icon-48x48.png": "aaf0463dfdb4339c74d72f97c68655d3",
"icons/apple-icon-76x76.png": "dabab77a6c0093963620ab6a7091e67c",
"icons/apple-icon-60x60.png": "bc129579c050a9260543e4d83dbefae0",
"icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"icons/android-icon-144x144.png": "fc1ca58ada975aca847c85e579f2fa3f",
"icons/apple-icon-72x72.png": "3b7d20a16dcb1bc82279ef16bbe4a5f3",
"icons/apple-icon-120x120.png": "6382d657066a207cc8daa39e5d584e69",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"icons/favicon-32x32.png": "f0ec9ddcbc3f35c565dd899037b0f7c8",
"icons/ms-icon-70x70.png": "67e27336f6d8582bac828203e43393e6",
"manifest.json": "1385c8c5b72532f1672d3a4c31822ea6",
"assets/AssetManifest.json": "328382620fe612c642c677e70a308477",
"assets/NOTICES": "b3a8e0deb037df1e631dd60fc4602167",
"assets/FontManifest.json": "d26fe437d4c6b4ef6a0da554d7769819",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"assets/packages/font_awesome_flutter/lib/fonts/fa-solid-900.ttf": "aa1ec80f1b30a51d64c72f669c1326a7",
"assets/packages/font_awesome_flutter/lib/fonts/fa-regular-400.ttf": "5178af1d278432bec8fc830d50996d6f",
"assets/packages/font_awesome_flutter/lib/fonts/fa-brands-400.ttf": "b37ae0f14cbc958316fac4635383b6e8",
"assets/fonts/MaterialIcons-Regular.otf": "95db9098c58fd6db106f1116bae85a0b",
"assets/assets/images/esa_opt_feature.jpg": "386e51976dcb0f9c300869acf10f1a38",
"assets/assets/images/img_esa_0801.jpg": "327955ec3151d1e121dededa6a18bf11",
"assets/assets/images/slide_1-layer_2.png": "ddbf15fa920dae7a5a686ff7d10d4841",
"assets/assets/images/esa-clients-logo.png": "116737c941954ace8ff4eb8ac0edeecb",
"assets/assets/images/slide_1-layer_1.png": "9063147efb1e199ff5c150d2e6300aa7",
"assets/assets/images/icon_ui.png": "09138184ad5cee139e01c3aba95924d4",
"assets/assets/images/slide_4-layer_1.png": "819a00d7172b9dbfe753dd0e58b6543d",
"assets/assets/images/icon_performance.png": "49dcd794c0838072d8cd729f1cb85e61",
"assets/assets/images/esa_consolidation2.jpg": "acb6ab0ab47ac8be7f94169751dbfd16",
"assets/assets/images/slide_4-layer_2.png": "bc967acf07078968e2cb2c06cbd8d1a1",
"assets/assets/images/esa_logo_bgblue_small.png": "a09aaab4349e2b103da011ed4cfdfd95",
"assets/assets/images/slide_1-bg.jpg": "7a2356f8cc1bf7d33376fac73ec04c64",
"assets/assets/images/news_flutter_ios_application_bundle.png": "c7d0eb7b38b5d44dabe759246f4fc376",
"assets/assets/images/img_modul_finance.png": "9914a98f5235efd3cbbabc5dd1aec15b",
"assets/assets/images/logo_bgputih.png": "855d72e69247f31142567973ac783ab9",
"assets/assets/images/icon_github_64x.png": "2d1e54d81bd7cb7daa09cea82dcb29e2",
"assets/assets/images/icon_development.png": "baebd28f59701e856c98c68e010fdeb2",
"assets/assets/images/slide_2-layer_1.png": "d74c30d69caf1be60b16532c70e5f9da",
"assets/assets/images/slide_2-layer_2.png": "9f38f12164b646341c610a77fd6c2e7a",
"assets/assets/images/slide_3-bg.jpg": "4eb412a5e5b66476b4b0ee84b51ce232",
"assets/assets/images/esa_cloud_intro.png": "ebe2e5a8efa43a166fc03631c8277595",
"assets/assets/images/flutter_logo_text.png": "d21f1eecaeaab081ba7efec1721c0712",
"assets/assets/images/icon_youtube_64x.png": "155d50c9f10c97fae27575ae7ee7b026",
"assets/assets/images/logo_bg2.png": "c40afee59e912009be98bd09f240dd5f",
"assets/assets/images/news_flutter_1.12.png": "1afef8f0d8e3ba20b49bc5625168b629",
"assets/assets/images/img_esa_0301.jpg": "5929b29cd5c74ca29aae392c57b39ce9",
"assets/assets/images/img_esa_100.png": "c1fd4f4f4e081b6438e30d3d92291e48",
"assets/assets/images/esa-clients-logo2.png": "08d88da75eb23dfe71df30ee01b65edc",
"assets/assets/images/esa_cloud_intro2.png": "31a918470905dcb0a005664384c06ee1",
"assets/assets/images/img_esa_0101.jpg": "05c4c97408372a5dab605ce29fc5e5e3",
"assets/assets/images/esa_consolidation.jpg": "3a5fcf59d6142dcc39df4785a2a2ba48",
"assets/assets/images/esa-clients.png": "3ff21ffcc96a5f106b458d04a6d8d7c5",
"assets/assets/images/google_logo.png": "22979cce8fc21c09b7f26e131045a400",
"assets/assets/images/esa_logo_bgblue.png": "385f0b7fe82f7eeee9dbfcb729fbc179",
"assets/assets/images/icon_search_64x.png": "f55e992d786ef1822db8d1c038e201b5",
"assets/assets/images/device_frame.png": "58e971455388b5c476887bf687a71d8a",
"assets/assets/images/slide_4-bg.jpg": "acbecb9abe325a9564a8ab511343cda9",
"assets/assets/images/video_thumbnail_learn_from_developers.png": "7818ad639493077ee21fbd29fccdc7cf",
"assets/assets/images/img_modul_ap.png": "e6c5ff279dde45ecb6aa0268cc534faa",
"assets/assets/images/flutter_logo_mono.png": "6261e07a1b1eb9cdd466b4879a41e361",
"assets/assets/images/slide_3-layer_2.png": "dcecc562f08c8e185fa50f23dec1c5e6",
"assets/assets/images/img_esa_01.jpg": "5330d4886d74aa5ce073e3eb76ad1b29",
"assets/assets/images/contact_us.jpg": "ea6010f80068348143de07c6b721224e",
"assets/assets/images/img_azure.jpg": "1264ee4aa1ab3bf0c2f1bef737aeddde",
"assets/assets/images/img_esa_03.jpg": "40e3bb3022569e6c636b3a3571e43de4",
"assets/assets/images/slide_3-layer_1.png": "0e3ed24ce06edf89fab5072c310d393d",
"assets/assets/images/img_esa_02.jpg": "406ab85a8cf436ad5f32bfa062dc2278",
"assets/assets/images/esa_modul.jpg": "cb6ad1937e775518bd95a63933e83211",
"assets/assets/images/img_modul_assets.png": "7ee0f8b16b920842b3112baa00d0fd00",
"assets/assets/images/img_modul_ar.png": "1e014d202a69723519dd9a90480fb865",
"assets/assets/images/slide_2-bg.jpg": "05ce416b9516bbecb0b3fa90c593fc5e",
"assets/assets/images/img_esa_06.jpg": "955ac971b0890693c90e178a48baf456",
"assets/assets/images/icon_twitter_64x.png": "514d54a802579a10b988c12dc892e9cb",
"assets/assets/images/logo_blue.png": "d6048169d9105ef5ab63a482d2a1c425",
"assets/assets/images/logo_erp.png": "f0a22a97cf1cdf61c801ec297856f911",
"assets/assets/images/img_esa_07.jpg": "967825e3b52b993327305e301a1506ae",
"assets/assets/images/img_modul_accounting.png": "d98df224aab1ac361d4ef05120f55a54",
"assets/assets/images/esa_cloud_2.png": "f2c19c892f1b6d88337f97a68786af31",
"assets/assets/images/img_esa_05.jpg": "4e72689df9c1956fb0142fc622998039",
"assets/assets/images/companies_using_flutter.png": "bd93766fea0502d711ec59e1e6aa29b9",
"assets/assets/images/img_esa_04.jpg": "c912ff36683082ecb70b3c0c840c4cba",
"assets/assets/images/news_flutter_codepen.png": "bc607f3054d4ab94ec5866422eb7c9c5",
"assets/assets/videos/NativePerformance.mp4": "91e263a32e6220fc72a57b587e2a4591",
"assets/assets/videos/BeautifulUI.mp4": "354dcbace8259d15719a4ef4c9e46e8a",
"assets/assets/videos/FastDev.mp4": "c2488f978dae74ca3bd13c38f2ce30cb",
"assets/assets/videos/FeatureMultiCompany.mp4": "5adb95d4064fe0ef31576eb677b10115",
"assets/assets/fonts/product_sans_regular.ttf": "eae9c18cee82a8a1a52e654911f8fe83",
"assets/assets/fonts/product_sans_italic.ttf": "e88ec18827526928e71407a24937825a",
"assets/assets/fonts/roboto_bold.ttf": "e07df86cef2e721115583d61d1fb68a6",
"assets/assets/fonts/product_sans_bold.ttf": "dba0c688b8d5ee09a1e214aebd5d25e4",
"assets/assets/fonts/roboto_regular.ttf": "11eabca2251325cfc5589c9c6fb57b46",
"assets/assets/fonts/roboto_italic.ttf": "a720f17aa773e493a7ebf8b08459e66c",
"canvaskit/canvaskit.js": "c2b4e5f3d7a3d82aed024e7249a78487",
"canvaskit/profiling/canvaskit.js": "ae2949af4efc61d28a4a80fffa1db900",
"canvaskit/profiling/canvaskit.wasm": "95e736ab31147d1b2c7b25f11d4c32cd",
"canvaskit/canvaskit.wasm": "4b83d89d9fecbea8ca46f2f760c5a9ba"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "main.dart.js",
"index.html",
"assets/NOTICES",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache.
        return response || fetch(event.request).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
