import 'package:flutter/material.dart';
import 'package:ekossystem/ui/block_wrapper.dart';
import 'package:ekossystem/ui/carousel/carousel.dart';
import 'package:responsive_framework/responsive_framework.dart';

import 'components/components.dart';
import 'ui/blocks.dart';
import 'package:ekossystem/components/google.dart';

// import 'package:pwa/client.dart' as pwa;
// import 'package:pwa/worker.dart';

void main() {
  // new pwa.Client();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, widget) => ResponsiveWrapper.builder(
          ClampingScrollWrapper.builder(context, widget!),
          defaultScale: true,
          minWidth: 480,
          defaultName: MOBILE,
          breakpoints: [
            const ResponsiveBreakpoint.autoScale(480, name: MOBILE),
            const ResponsiveBreakpoint.resize(600, name: MOBILE),
            const ResponsiveBreakpoint.resize(850, name: TABLET),
            const ResponsiveBreakpoint.resize(1080, name: DESKTOP),
          ],
          background: Container(color: background)),
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideDrawer(),
      appBar: PreferredSize(
          preferredSize: Size(double.infinity, 66),
          child: AppBar(
            automaticallyImplyLeading: false, // hides leading widget
            leading: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
                ? Builder(builder: (BuildContext context) {
                    return IconButton(
                        icon: Icon(Icons.menu, color: textPrimary, size: 28),
                        onPressed: () {
                          Scaffold.of(context).openDrawer();
                        });
                  })
                : Text(''),
            backgroundColor: Colors.white,
            title: Container(
              child: MenuBar(),
            ),
          )),
      body: ListView.builder(
          itemCount: blocks.length,
          itemBuilder: (context, index) {
            return blocks[index];
          }),
    );
  }
}

List<Widget> blocks = [
  // ResponsiveWrapper(
  //     maxWidth: 1200,
  //     minWidth: 1200,
  //     defaultScale: true,
  //     mediaQueryData: const MediaQueryData(size: Size(1200, 640)),
  //     child: RepaintBoundary(child: Carousel())),
  // const BlockWrapper(GetSlideshow()),
  // const BlockWrapper(Features()),
  const BlockWrapper(Intro()),
  const BlockWrapper(FastDevelopment()),
  // const BlockWrapper(GetStarted()),
  const BlockWrapper(BeautifulUI()),
  const BlockWrapper(NativePerformance()),
  const BlockWrapper(BeautifulUI2()),
  const BlockWrapper(FastDevelopment2()),
  // const BlockWrapper(LearnFromDevelopers()),
  const BlockWrapper(WhoUsesFlutter()),
  // const BlockWrapper(FlutterNewsRow()),
  // const BlockWrapper(InstallFlutter()),
  const Footer(),
  // const BlockWrapper(maps()),
];

// Disabled codelab block for performance.
//              ResponsiveVisibility(
//                hiddenWhen: [Condition.smallerThan(name: DESKTOP)],
//                child: ResponsiveConstraints(
//                    constraintsWhen: blockWidthConstraints,
//                    child: FlutterCodelab()),
//              ),

class AboutUs extends StatelessWidget {
  //const AboutUs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, widget) => ResponsiveWrapper.builder(
          ClampingScrollWrapper.builder(context, widget!),
          defaultScale: true,
          minWidth: 480,
          defaultName: MOBILE,
          breakpoints: [
            const ResponsiveBreakpoint.autoScale(480, name: MOBILE),
            const ResponsiveBreakpoint.resize(600, name: MOBILE),
            const ResponsiveBreakpoint.resize(850, name: TABLET),
            const ResponsiveBreakpoint.resize(1080, name: DESKTOP),
          ],
          background: Container(color: background)),
      home: HomeAboutUs(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class HomeAboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideDrawer(),
      appBar: PreferredSize(
          preferredSize: Size(double.infinity, 66),
          child: AppBar(
            automaticallyImplyLeading: false, // hides leading widget
            leading: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
                ? Builder(builder: (BuildContext context) {
                    return IconButton(
                        icon: Icon(Icons.menu, color: textPrimary, size: 28),
                        onPressed: () {
                          Scaffold.of(context).openDrawer();
                        });
                  })
                : Text(''),
            backgroundColor: Colors.white,
            title: Container(
              child: MenuBar(),
            ),
          )),
      body: ListView.builder(
          itemCount: blocksAboutUs.length,
          itemBuilder: (context, index) {
            return blocksAboutUs[index];
          }),
    );
  }
}

List<Widget> blocksAboutUs = [
  // const BlockWrapper(GetStarted()),
  const BlockWrapper(IntroAboutUs()),
  const BlockWrapper(FastDevelopmentAboutUs()),
];

// class Services extends StatelessWidget {
//   const Services({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       builder: (context, widget) => ResponsiveWrapper.builder(
//           ClampingScrollWrapper.builder(context, widget!),
//           defaultScale: true,
//           minWidth: 480,
//           defaultName: MOBILE,
//           breakpoints: [
//             const ResponsiveBreakpoint.autoScale(480, name: MOBILE),
//             const ResponsiveBreakpoint.resize(600, name: MOBILE),
//             const ResponsiveBreakpoint.resize(850, name: TABLET),
//             const ResponsiveBreakpoint.resize(1080, name: DESKTOP),
//           ],
//           background: Container(color: background)),
//       home: HomeServices(),
//       debugShowCheckedModeBanner: false,
//     );
//   }
// }

// class HomeServices extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       drawer: SideDrawer(),
//       appBar: PreferredSize(
//           preferredSize: Size(double.infinity, 66),
//           child: AppBar(
//             automaticallyImplyLeading: false, // hides leading widget
//             leading: Builder(builder: (BuildContext context) {
//               return IconButton(
//                   icon: Icon(Icons.menu, color: textPrimary, size: 28),
//                   onPressed: () {
//                     Scaffold.of(context).openDrawer();
//                   });
//             }),
//             backgroundColor: Colors.white,
//             title: Container(
//               child: MenuBar(),
//             ),
//           )),
//       body: ListView.builder(
//             itemCount: blocksServices.length,
//             itemBuilder: (context, index) {
//               return blocksServices[index];
//             }),
//     );
//   }
// }

// List<Widget> blocksServices = [
//   const BlockWrapper(FastDevelopment()),
//   const BlockWrapper(BeautifulUI()),
//   const BlockWrapper(NativePerformance()),
//   const BlockWrapper(BeautifulUI2()),
//   const BlockWrapper(FastDevelopment2()),
// ];

class Clients extends StatelessWidget {
  const Clients({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, widget) => ResponsiveWrapper.builder(
          ClampingScrollWrapper.builder(context, widget!),
          defaultScale: true,
          minWidth: 480,
          defaultName: MOBILE,
          breakpoints: [
            const ResponsiveBreakpoint.autoScale(480, name: MOBILE),
            const ResponsiveBreakpoint.resize(600, name: MOBILE),
            const ResponsiveBreakpoint.resize(850, name: TABLET),
            const ResponsiveBreakpoint.resize(1080, name: DESKTOP),
          ],
          background: Container(color: background)),
      home: HomeClients(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class HomeClients extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideDrawer(),
      appBar: PreferredSize(
          preferredSize: Size(double.infinity, 66),
          child: AppBar(
            automaticallyImplyLeading: false, // hides leading widget
            leading: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
                ? Builder(builder: (BuildContext context) {
                    return IconButton(
                        icon: Icon(Icons.menu, color: textPrimary, size: 28),
                        onPressed: () {
                          Scaffold.of(context).openDrawer();
                        });
                  })
                : Text(''),
            backgroundColor: Colors.white,
            title: Container(
              child: MenuBar(),
            ),
          )),
      body: ListView.builder(
          itemCount: blocksClients.length,
          itemBuilder: (context, index) {
            return blocksClients[index];
          }),
    );
  }
}

List<Widget> blocksClients = [
  const BlockWrapper(WhoUsesFlutter()),
];

gotoHome(BuildContext context) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => MyApp()),
  );
}

gotoAbout(BuildContext context) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => AboutUs()),
  );
}

gotoServices(BuildContext context) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Readmore()),
  );
}

gotoClient(BuildContext context) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Clients()),
  );
}

gotoContact(BuildContext context) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Contact()),
  );
}

class SideDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 100,
            child: DrawerHeader(
              child: Image.asset("assets/images/logo_erp.png",
                  height: 120, width: 100),
            ),
          ),
          ListTile(
            // leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: () {
              gotoHome(context);
            },
          ),
          ListTile(
            // leading: Icon(Icons.home),
            title: Text('About'),
            onTap: () {
              gotoAbout(context);
            },
          ),
          ListTile(
            // leading: Icon(Icons.home),
            title: Text('Service/Products'),
            onTap: () {
              gotoServices(context);
            },
          ),
          ListTile(
            // leading: Icon(Icons.home),
            title: Text('Client'),
            onTap: () {
              gotoClient(context);
            },
          ),
          ListTile(
            // leading: Icon(Icons.home),
            title: Text('Contact'),
            onTap: () {
              gotoContact(context);
            },
          ),
        ],
      ),
    );
  }
}

class Contact extends StatelessWidget {
  const Contact({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, widget) => ResponsiveWrapper.builder(
          ClampingScrollWrapper.builder(context, widget!),
          defaultScale: true,
          minWidth: 480,
          defaultName: MOBILE,
          breakpoints: [
            const ResponsiveBreakpoint.autoScale(480, name: MOBILE),
            const ResponsiveBreakpoint.resize(600, name: MOBILE),
            const ResponsiveBreakpoint.resize(850, name: TABLET),
            const ResponsiveBreakpoint.resize(1080, name: DESKTOP),
          ],
          background: Container(color: background)),
      home: HomeContact(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class HomeContact extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideDrawer(),
      appBar: PreferredSize(
          preferredSize: Size(double.infinity, 66),
          child: AppBar(
            automaticallyImplyLeading: false, // hides leading widget
            leading: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
                ? Builder(builder: (BuildContext context) {
                    return IconButton(
                        icon: Icon(Icons.menu, color: textPrimary, size: 28),
                        onPressed: () {
                          Scaffold.of(context).openDrawer();
                        });
                  })
                : Text(''),
            backgroundColor: Colors.white,
            title: Container(
              child: MenuBar(),
            ),
          )),
      body: ListView.builder(
          itemCount: blocksContactUs.length,
          itemBuilder: (context, index) {
            return blocksContactUs[index];
          }),
    );
  }
}

List<Widget> blocksContactUs = [
  const BlockWrapper(ContactUs()),
];

class Readmore extends StatelessWidget {
  const Readmore({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, widget) => ResponsiveWrapper.builder(
          ClampingScrollWrapper.builder(context, widget!),
          defaultScale: true,
          minWidth: 480,
          defaultName: MOBILE,
          breakpoints: [
            const ResponsiveBreakpoint.autoScale(480, name: MOBILE),
            const ResponsiveBreakpoint.resize(600, name: MOBILE),
            const ResponsiveBreakpoint.resize(850, name: TABLET),
            const ResponsiveBreakpoint.resize(1080, name: DESKTOP),
          ],
          background: Container(color: background)),
      home: HomeReadmore(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class HomeReadmore extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideDrawer(),
      appBar: PreferredSize(
          preferredSize: Size(double.infinity, 66),
          child: AppBar(
            automaticallyImplyLeading: false, // hides leading widget
            leading: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
                ? Builder(builder: (BuildContext context) {
                    return IconButton(
                        icon: Icon(Icons.menu, color: textPrimary, size: 28),
                        onPressed: () {
                          Scaffold.of(context).openDrawer();
                        });
                  })
                : Text(''),
            backgroundColor: Colors.white,
            title: Container(
              child: MenuBar(),
            ),
          )),
      body: ListView.builder(
          itemCount: blocksReadmore.length,
          itemBuilder: (context, index) {
            return blocksReadmore[index];
          }),
    );
  }
}

List<Widget> blocksReadmore = [
  const BlockWrapper(DetailModul()),
  const BlockWrapper(DetailModul2()),
  const BlockWrapper(DetailModul3()),
  const BlockWrapper(DetailModul4()),
  const BlockWrapper(DetailModul5()),
  const BlockWrapper(DetailModul6()),
  const BlockWrapper(DetailModul7()),
  const BlockWrapper(DetailModul8()),
  const BlockWrapper(DetailModul9()),
  const BlockWrapper(DetailModul10()),
  const BlockWrapper(DetailModul11()),
];
