import 'dart:ui' as ui;

import 'package:ekossystem/components/google.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:ekossystem/components/components.dart';
import 'package:ekossystem/utils/utils.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:universal_html/html.dart' as html;
import 'package:video_player/video_player.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:ekossystem/components/flutter_image_slideshow.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../main.dart';
import 'package:ekossystem/components/google.dart';

class MenuBar extends StatelessWidget {
  const MenuBar({Key? key}) : super(key: key);

  gotoHome(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MyApp()),
    );
  }

  gotoAbout(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AboutUs()),
    );
  }

  gotoServices(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Readmore()),
    );
  }

  gotoClient(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Clients()),
    );
  }

  gotoContact(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Contact()),
    );
  }

  @override
  Widget build(BuildContext context) {
    const Color navLinkColor = Color(0xFF6E7274);
    return Container(
      height: 66,
      decoration: const BoxDecoration(
        color: Colors.white,
        // boxShadow: [
        //   BoxShadow(color: Color(0x1A000000), offset: Offset(0, 2), blurRadius: 4)
        // ]
      ),
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Row(
        children: <Widget>[
          MouseRegion(
            cursor: SystemMouseCursors.click,
            child: GestureDetector(
              onTap: () =>
                  Navigator.of(context).popUntil((route) => route.isFirst),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0, 5, 16, 5),
                child: Image.asset("assets/images/logo_erp.png",
                    height: 37, fit: BoxFit.contain),
              ),
            ),
          ),
          const Spacer(),
          ResponsiveVisibility(
            visible: false,
            visibleWhen: const [Condition.largerThan(name: MOBILE)],
            child: MouseRegion(
              cursor: SystemMouseCursors.click,
              child: GestureDetector(
                // onTap: () => openUrl("/#"),
                onTap: () {
                  gotoHome(context);
                },
                child: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Text("Home",
                      style: TextStyle(
                          fontSize: 16,
                          color: navLinkColor,
                          fontFamily: fontFamily)),
                ),
              ),
            ),
          ),
          ResponsiveVisibility(
            visible: false,
            visibleWhen: const [Condition.largerThan(name: MOBILE)],
            child: MouseRegion(
              cursor: SystemMouseCursors.click,
              child: GestureDetector(
                // onTap: () => openUrl("/#"),
                onTap: () {
                  gotoAbout(context);
                },
                child: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Text("About",
                      style: TextStyle(
                          fontSize: 16,
                          color: navLinkColor,
                          fontFamily: fontFamily)),
                ),
              ),
            ),
          ),
          ResponsiveVisibility(
            visible: false,
            visibleWhen: const [Condition.largerThan(name: MOBILE)],
            child: MouseRegion(
              cursor: SystemMouseCursors.click,
              child: GestureDetector(
                // onTap: () => openUrl("/#"),
                onTap: () {
                  gotoServices(context);
                },
                child: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Text("Service/Products",
                      style: TextStyle(
                          fontSize: 16,
                          color: navLinkColor,
                          fontFamily: fontFamily)),
                ),
              ),
            ),
          ),
          ResponsiveVisibility(
            visible: false,
            visibleWhen: const [Condition.largerThan(name: MOBILE)],
            child: MouseRegion(
              cursor: SystemMouseCursors.click,
              child: GestureDetector(
                // onTap: () => openUrl("/#"),
                onTap: () {
                  gotoClient(context);
                },
                child: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Text("Client",
                      style: TextStyle(
                          fontSize: 16,
                          color: navLinkColor,
                          fontFamily: fontFamily)),
                ),
              ),
            ),
          ),
          ResponsiveVisibility(
            visible: false,
            visibleWhen: const [Condition.largerThan(name: MOBILE)],
            child: MouseRegion(
              cursor: SystemMouseCursors.click,
              child: GestureDetector(
                // onTap: () => openUrl("/#"),
                onTap: () {
                  gotoContact(context);
                },
                child: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Text("Contact",
                      style: TextStyle(
                          fontSize: 16,
                          color: navLinkColor,
                          fontFamily: fontFamily)),
                ),
              ),
            ),
          ),

          // ResponsiveVisibility(
          //   visible: false,
          //   visibleWhen: const [Condition.largerThan(name: MOBILE)],
          //   child: MouseRegion(
          //     cursor: SystemMouseCursors.click,
          //     child: GestureDetector(
          //       onTap: () => openUrl("https://flutter.dev/docs"),
          //       child: const Padding(
          //         padding: EdgeInsets.symmetric(horizontal: 16),
          //         child: Text("Docs",
          //             style: TextStyle(
          //                 fontSize: 16,
          //                 color: navLinkColor,
          //                 fontFamily: fontFamily)),
          //       ),
          //     ),
          //   ),
          // ),
          // ResponsiveVisibility(
          //   visible: false,
          //   visibleWhen: const [Condition.largerThan(name: MOBILE)],
          //   child: MouseRegion(
          //     cursor: SystemMouseCursors.click,
          //     child: GestureDetector(
          //       onTap: () => openUrl("https://flutter.dev/showcase"),
          //       child: const Padding(
          //         padding: EdgeInsets.symmetric(horizontal: 16),
          //         child: Text("Showcase",
          //             style: TextStyle(
          //                 fontSize: 16,
          //                 color: navLinkColor,
          //                 fontFamily: fontFamily)),
          //       ),
          //     ),
          //   ),
          // ),
          // ResponsiveVisibility(
          //   visible: false,
          //   visibleWhen: const [Condition.largerThan(name: MOBILE)],
          //   child: MouseRegion(
          //     cursor: SystemMouseCursors.click,
          //     child: GestureDetector(
          //       onTap: () => openUrl("https://flutter.dev/community"),
          //       child: const Padding(
          //           padding: EdgeInsets.symmetric(horizontal: 16),
          //           child: Text("Community",
          //               style: TextStyle(
          //                   fontSize: 16,
          //                   color: navLinkColor,
          //                   fontFamily: fontFamily))),
          //     ),
          //   ),
          // ),
          // const ResponsiveVisibility(
          //   visible: false,
          //   visibleWhen: [Condition.largerThan(name: MOBILE)],
          //   child: MouseRegion(
          //     cursor: SystemMouseCursors.click,
          //     child: Padding(
          //       padding: EdgeInsets.symmetric(horizontal: 18),
          //       child: ImageIcon(
          //           AssetImage("assets/images/icon_search_64x.png"),
          //           color: navLinkColor,
          //           size: 24),
          //     ),
          //   ),
          // ),
          // MouseRegion(
          //   cursor: SystemMouseCursors.click,
          //   child: GestureDetector(
          //     onTap: () => openUrl('https://twitter.com/flutterdev'),
          //     child: const Padding(
          //       padding: EdgeInsets.symmetric(horizontal: 8),
          //       child: ImageIcon(
          //           AssetImage("assets/images/icon_twitter_64x.png"),
          //           color: navLinkColor,
          //           size: 24),
          //     ),
          //   ),
          // ),
          // MouseRegion(
          //   cursor: SystemMouseCursors.click,
          //   child: GestureDetector(
          //     onTap: () => openUrl('https://www.youtube.com/flutterdev'),
          //     child: const Padding(
          //       padding: EdgeInsets.symmetric(horizontal: 8),
          //       child: ImageIcon(
          //           AssetImage("assets/images/icon_youtube_64x.png"),
          //           color: navLinkColor,
          //           size: 24),
          //     ),
          //   ),
          // ),
          // MouseRegion(
          //   cursor: SystemMouseCursors.click,
          //   child: GestureDetector(
          //     onTap: () => openUrl('https://github.com/flutter'),
          //     child: const Padding(
          //       padding: EdgeInsets.symmetric(horizontal: 8),
          //       child: ImageIcon(
          //           AssetImage("assets/images/icon_github_64x.png"),
          //           color: navLinkColor,
          //           size: 24),
          //     ),
          //   ),
          // ),
          // ResponsiveVisibility(
          //   visible: false,
          //   visibleWhen: const [Condition.largerThan(name: MOBILE)],
          //   child: Padding(
          //     padding: const EdgeInsets.only(left: 8, right: 0),
          //     child: TextButton(
          //       onPressed: () =>
          //           openUrl("https://flutter.dev/docs/get-started/install"),
          //       style: ButtonStyle(
          //           backgroundColor: MaterialStateProperty.all<Color>(primary),
          //           overlayColor: MaterialStateProperty.resolveWith<Color>(
          //             (Set<MaterialState> states) {
          //               if (states.contains(MaterialState.hovered)) {
          //                 return buttonPrimaryDark;
          //               }
          //               if (states.contains(MaterialState.focused) ||
          //                   states.contains(MaterialState.pressed)) {
          //                 return buttonPrimaryDarkPressed;
          //               }
          //               return primary;
          //             },
          //           ),
          //           // Shape sets the border radius from default 3 to 0.
          //           shape: MaterialStateProperty.resolveWith<OutlinedBorder>(
          //             (Set<MaterialState> states) {
          //               if (states.contains(MaterialState.focused) ||
          //                   states.contains(MaterialState.pressed)) {
          //                 return const RoundedRectangleBorder(
          //                     borderRadius:
          //                         BorderRadius.all(Radius.circular(0)));
          //               }
          //               return const RoundedRectangleBorder(
          //                   borderRadius: BorderRadius.all(Radius.circular(0)));
          //             },
          //           ),
          //           padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
          //               const EdgeInsets.symmetric(
          //                   vertical: 22, horizontal: 28)),
          //           // Side adds pressed highlight outline.
          //           side: MaterialStateProperty.resolveWith<BorderSide>(
          //               (Set<MaterialState> states) {
          //             if (states.contains(MaterialState.focused) ||
          //                 states.contains(MaterialState.pressed)) {
          //               return const BorderSide(
          //                   width: 3, color: buttonPrimaryPressedOutline);
          //             }
          //             // Transparent border placeholder as Flutter does not allow
          //             // negative margins.
          //             return const BorderSide(width: 3, color: Colors.white);
          //           })),
          //       child: Text(
          //         "Get started",
          //         style: buttonTextStyle.copyWith(
          //             fontSize: 16, fontWeight: FontWeight.bold),
          //       ),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}

// class FeaturesScreen extends StatelessWidget {

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Second Activity Screen"),
//       ),
//       body: Center(
//         child: Text('Go Back To Previous Screen'),
//         ),
//     );
//   }
// }

//-----------------------------------------------------------------------------------
class GetSlideshow extends StatelessWidget {
  const GetSlideshow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ImageSlideshow(
      /// Width of the [ImageSlideshow].
      // width: 1260,
      // width: 600,
      width: ResponsiveWrapper.of(context).isMobile ? 600 : 1260,

      /// Height of the [ImageSlideshow].
      // height: 600,
      // height: 400,
      height: ResponsiveWrapper.of(context).isMobile ? 400 : 600,

      /// The page to show when first creating the [ImageSlideshow].
      initialPage: 0,

      /// The color to paint the indicator.
      indicatorColor: Colors.blue,

      /// The color to paint behind th indicator.
      indicatorBackgroundColor: Colors.grey,

      /// The widgets to display in the [ImageSlideshow].
      /// Add the sample image file into the images folder
      /// Image.asset("assets/images/slide_1-bg.jpg", fit: BoxFit.fill),
      children: [
        // Image.asset(
        //   'assets/images/img_esa_07.jpg',
        //   fit: BoxFit.cover,
        // ),
        Image.asset(
          'assets/images/img_esa_01.jpg',
          fit: BoxFit.cover,
        ),
        Image.asset(
          'assets/images/img_esa_02.jpg',
          fit: BoxFit.cover,
        ),
        Image.asset(
          'assets/images/img_esa_03.jpg',
          fit: BoxFit.cover,
        ),
        Image.asset(
          'assets/images/img_esa_04.jpg',
          fit: BoxFit.cover,
        ),
        Image.asset(
          'assets/images/img_esa_05.jpg',
          fit: BoxFit.cover,
        ),
        Image.asset(
          'assets/images/img_esa_06.jpg',
          fit: BoxFit.cover,
        ),
      ],

      /// Called whenever the page in the center of the viewport changes.
      onPageChanged: (value) {
        print('Page changed: $value');
      },

      /// Auto scroll interval.
      /// Do not auto scroll with null or 0.
      autoPlayInterval: 5000,

      /// Loops back to first slide.
      isLoop: true,
    );
  }
}
//-----------------------------------------------------------------------------------

class GetStarted extends StatelessWidget {
  const GetStarted({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: const EdgeInsets.all(40),
      child: Align(
        alignment: Alignment.center,
        child: Container(
          constraints: const BoxConstraints(maxWidth: 780),
          child: Column(
            children: [
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.center,
              //   crossAxisAlignment: CrossAxisAlignment.center,
              //   children: [
              //     Text("Tentang EkosSystem ",
              //         style: headlineSecondaryTextStyle.copyWith(fontSize: 24)),
              //     Padding(
              //       padding: const EdgeInsets.only(top: 5),
              //       child: Image.asset("assets/images/logo_bgputih.png",
              //           // width: 75, height: 24, fit: BoxFit.fill),
              //           width: 50, height: 49, fit: BoxFit.fill),
              //     ),
              //   ],
              // ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 32),
                child: RichText(
                  text: TextSpan(
                    children: [
                      // const TextSpan(
                      //     text:
                      //         "Flutter is Google’s UI toolkit for building beautiful, natively compiled applications for ",
                      //     style: headlineSecondaryTextStyle),
                      const TextSpan(text: "", style: bodyTextStyle),
                      TextSpan(
                          text: "Ekossystem (PT. Ekos Sistem Aplikasi)",
                          style: bodyTextStyle.copyWith(color: primary)),
                      // const TextSpan(
                      //     text: ", ", style: headlineSecondaryTextStyle),
                      // TextSpan(
                      //     recognizer: TapGestureRecognizer()
                      //       ..onTap = () {
                      //         openUrl("https://flutter.dev/web");
                      //       },
                      //     text: "web",
                      //     style: headlineSecondaryTextStyle.copyWith(
                      //         color: primary)),
                      // const TextSpan(
                      //     text: ", and ", style: headlineSecondaryTextStyle),
                      // TextSpan(
                      //     recognizer: TapGestureRecognizer()
                      //       ..onTap = () {
                      //         openUrl("https://flutter.dev/desktop");
                      //       },
                      //     text: "desktop",
                      //     style: headlineSecondaryTextStyle.copyWith(
                      //         color: primary)),
                      TextSpan(
                          text: " adalah perusahaan pengembang software  aplikasi yang berkedudukan di Jakarta Selatan - Indonesia." +
                              "\n\nEkossystem memiliki produk unggulan berupa sistem ERP (Enterprise Resources Planning) yaitu software aplikasi bisnis yang mengintegrasikan sebagian besar proses/fungsi bisnis perusahaan,  melibatkan berbagai unit fungsional/departement di dalam perusahaan,  dan merupakan sebuah sistem yang sesuai dengan standard Akuntansi (PSAK) yang berlaku di Indonesia." +
                              "\n\nERP ekossystem menggunakan tekhnologi terkini, berbasis cloud bisa diakses dari manapun melalui multi-platform." +
                              "\n\nEkossystem juga membuat modul software baru sesuai pesanan atau melakukan kustomisasi untuk klien." +
                              "\n\nEkossystem memberikan pilihan solusi sistem ERP yang tepat guna dengan kualitas bersaing.",
                          style: bodyTextStyle),
                    ],
                  ),
                  textAlign: ui.TextAlign.left,
                ),
              ),
              // Padding(
              //   padding: const EdgeInsets.only(bottom: 32),
              //   child: ResponsiveRowColumn(
              //     layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
              //         ? ResponsiveRowColumnType.COLUMN
              //         : ResponsiveRowColumnType.ROW,
              //     rowMainAxisAlignment: MainAxisAlignment.center,
              //     rowCrossAxisAlignment: CrossAxisAlignment.center,
              //     children: [
              //       ResponsiveRowColumnItem(
              //         child: TextButton(
              //           onPressed: () => openUrl(
              //               "#"),
              //           style: ButtonStyle(
              //               backgroundColor:
              //                   MaterialStateProperty.all<Color>(primary),
              //               overlayColor:
              //                   MaterialStateProperty.resolveWith<Color>(
              //                 (Set<MaterialState> states) {
              //                   if (states.contains(MaterialState.hovered)) {
              //                     return buttonPrimaryDark;
              //                   }
              //                   if (states.contains(MaterialState.focused) ||
              //                       states.contains(MaterialState.pressed)) {
              //                     return buttonPrimaryDarkPressed;
              //                   }
              //                   return primary;
              //                 },
              //               ),
              //               // Shape sets the border radius from default 3 to 0.
              //               shape: MaterialStateProperty.resolveWith<
              //                   OutlinedBorder>(
              //                 (Set<MaterialState> states) {
              //                   if (states.contains(MaterialState.focused) ||
              //                       states.contains(MaterialState.pressed)) {
              //                     return const RoundedRectangleBorder(
              //                         borderRadius:
              //                             BorderRadius.all(Radius.circular(0)));
              //                   }
              //                   return const RoundedRectangleBorder(
              //                       borderRadius:
              //                           BorderRadius.all(Radius.circular(0)));
              //                 },
              //               ),
              //               padding:
              //                   MaterialStateProperty.all<EdgeInsetsGeometry>(
              //                       const EdgeInsets.symmetric(
              //                           vertical: 32, horizontal: 84)),
              //               // Side adds pressed highlight outline.
              //               side: MaterialStateProperty.resolveWith<BorderSide>(
              //                   (Set<MaterialState> states) {
              //                 if (states.contains(MaterialState.focused) ||
              //                     states.contains(MaterialState.pressed)) {
              //                   return const BorderSide(
              //                       width: 3,
              //                       color: buttonPrimaryPressedOutline);
              //                 }
              //                 // Transparent border placeholder as Flutter does not allow
              //                 // negative margins.
              //                 return const BorderSide(
              //                     width: 3, color: Colors.white);
              //               })),
              //           child: Text(
              //             "Contact Us",
              //             style: buttonTextStyle.copyWith(fontSize: 18),
              //           ),
              //         ),
              //       ),
              //       // ResponsiveRowColumnItem(
              //       //   child: Padding(
              //       //     padding: const EdgeInsets.symmetric(horizontal: 8),
              //       //     child: TextButton(
              //       //       onPressed: () => {},
              //       //       style: TextButton.styleFrom(
              //       //         shape: const RoundedRectangleBorder(
              //       //             borderRadius:
              //       //                 BorderRadius.all(Radius.circular(0))),
              //       //         padding: const EdgeInsets.symmetric(
              //       //             vertical: 20, horizontal: 20),
              //       //       ),
              //       //       child: Row(
              //       //         mainAxisSize: MainAxisSize.min,
              //       //         children: [
              //       //           const Padding(
              //       //             padding: EdgeInsets.only(right: 8),
              //       //             child: Icon(
              //       //               Icons.play_circle_filled,
              //       //               size: 24,
              //       //               color: primary,
              //       //             ),
              //       //           ),
              //       //           Text(
              //       //             "Watch video",
              //       //             style: buttonTextStyle.copyWith(
              //       //                 fontSize: 16, color: primary),
              //       //           ),
              //       //         ],
              //       //       ),
              //       //     ),
              //       //   ),
              //       // )
              //     ],
              //   ),
              // ),
              // Padding(
              //   padding: const EdgeInsets.only(bottom: 16),
              //   child: RichText(
              //     text: TextSpan(children: [
              //       const TextSpan(
              //           text: "Coming from another platform? Docs: ",
              //           style: bodyTextStyle),
              //       TextSpan(
              //           recognizer: TapGestureRecognizer()
              //             ..onTap = () {
              //               openUrl(
              //                   "https://flutter.dev/docs/get-started/flutter-for/ios-devs");
              //             },
              //           text: "iOS",
              //           style: bodyLinkTextStyle),
              //       const TextSpan(text: ", ", style: bodyTextStyle),
              //       TextSpan(
              //           recognizer: TapGestureRecognizer()
              //             ..onTap = () {
              //               openUrl(
              //                   "https://flutter.dev/docs/get-started/flutter-for/android-devs");
              //             },
              //           text: "Android",
              //           style: bodyLinkTextStyle),
              //       const TextSpan(text: ", ", style: bodyTextStyle),
              //       TextSpan(
              //           recognizer: TapGestureRecognizer()
              //             ..onTap = () {
              //               openUrl(
              //                   "https://flutter.dev/docs/get-started/flutter-for/web-devs");
              //             },
              //           text: "Web",
              //           style: bodyLinkTextStyle),
              //       const TextSpan(text: ", ", style: bodyTextStyle),
              //       TextSpan(
              //           recognizer: TapGestureRecognizer()
              //             ..onTap = () {
              //               openUrl(
              //                   "https://flutter.dev/docs/get-started/flutter-for/react-native-devs");
              //             },
              //           text: "React Native",
              //           style: bodyLinkTextStyle),
              //       const TextSpan(text: ", ", style: bodyTextStyle),
              //       TextSpan(
              //           recognizer: TapGestureRecognizer()
              //             ..onTap = () {
              //               openUrl(
              //                   "https://flutter.dev/docs/get-started/flutter-for/xamarin-forms-devs");
              //             },
              //           text: "Xamarin",
              //           style: bodyLinkTextStyle),
              //       const TextSpan(text: ".", style: bodyTextStyle),
              //     ]),
              //     textAlign: TextAlign.center,
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}

class Features extends StatelessWidget {
  const Features({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        columnCrossAxisAlignment: CrossAxisAlignment.center,
        columnMainAxisSize: MainAxisSize.min,
        rowPadding: const EdgeInsets.symmetric(horizontal: 80, vertical: 80),
        columnPadding: const EdgeInsets.symmetric(horizontal: 25, vertical: 50),
        columnSpacing: 50,
        rowSpacing: 50,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 1,
            rowFit: FlexFit.tight,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 32),
                  child: buildMaterialIconCircle(
                      "assets/images/icon_development.png", 68),
                ),
                const Padding(
                  padding: EdgeInsets.only(bottom: 16),
                  child: Text("Sistem Cloud",
                      style: headlineSecondaryTextStyle,
                      textAlign: TextAlign.center),
                ),
                const Text(
                    "ERP ekossystem menggunakan tekhnologi terkini, berbasis cloud bisa diakses dari manapun melalui multi-platform.",
                    style: bodyTextStyle,
                    textAlign: TextAlign.center),
              ],
            ),
          ),
          ResponsiveRowColumnItem(
            rowFlex: 1,
            rowFit: FlexFit.tight,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 32),
                  child:
                      buildMaterialIconCircle("assets/images/icon_ui.png", 68),
                ),
                const Padding(
                  padding: EdgeInsets.only(bottom: 16),
                  child: Text("Kustomisasi Modul",
                      style: headlineSecondaryTextStyle,
                      textAlign: TextAlign.center),
                ),
                const Text(
                    "Ekossystem juga membuat modul software baru sesuai pesanan atau melakukan kustomisasi untuk klien.",
                    style: bodyTextStyle,
                    textAlign: TextAlign.center),
              ],
            ),
          ),
          ResponsiveRowColumnItem(
            rowFlex: 1,
            rowFit: FlexFit.tight,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 32),
                  child: buildMaterialIconCircle(
                      "assets/images/icon_performance.png", 68),
                ),
                const Padding(
                  padding: EdgeInsets.only(bottom: 16),
                  child: Text("Solusi Tepat Guna",
                      style: headlineSecondaryTextStyle,
                      textAlign: TextAlign.center),
                ),
                RichText(
                  text: TextSpan(
                    style: bodyTextStyle,
                    children: [
                      const TextSpan(
                          text:
                              "Ekossystem memberikan pilihan solusi sistem ERP yang tepat guna dengan kualitas  dan harga bersaing."),
                      // TextSpan(
                      //     recognizer: TapGestureRecognizer()
                      //       ..onTap = () {
                      //         openUrl("https://dart.dev/platforms");
                      //       },
                      //     text: "Dart's native compilers",
                      //     style: bodyLinkTextStyle),
                      // const TextSpan(text: "."),
                    ],
                  ),
                  textAlign: TextAlign.center,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

//----------------------------------------------------------------
class Intro extends StatefulWidget {
  const Intro({Key? key}) : super(key: key);

  @override
  _IntroState createState() => _IntroState();
}

class _IntroState extends State<Intro> {
  late VideoPlayerController videoController;
  late Future<void> initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    videoController = VideoPlayerController.asset("assets/videos/FastDev.mp4");
    videoController.setVolume(0);
    videoController.setLooping(true);
    initializeVideoPlayerFuture = videoController.initialize().then((_) {
      if (mounted) {
        setState(() {});
        videoPlay();
      }
    });
  }

  @override
  void dispose() {
    videoController.dispose();
    super.dispose();
  }

  void videoPlay() {
    videoController.play();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      // padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 2,
            child: FutureBuilder(
              future: initializeVideoPlayerFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return AspectRatio(
                    aspectRatio: videoController.value.aspectRatio,
                    child: RepaintBoundary(
                        child:
                            Image.asset("assets/images/esa_cloud_intro.png")),
                  );
                } else {
                  return Container();
                }
              },
            ),
          ),
          ResponsiveRowColumnItem(
            rowFlex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 32, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // const Padding(
                  //   padding: EdgeInsets.only(bottom: 32),
                  //   child: Text("1. Ringkasan (Overview)", style: headlineTextStyle),
                  // ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text: "ERP EkosSystem Ini adalah software aplikasi berbasis cloud,  diakses via internet, bisa dari lokasi mana pun dan komputer apapun (IOS, Windows, Android), tidak perlu Instalasi, cukup buka Internet browser (seperti Goggle CHROME, Mozilla FIREFOX) kemudian ketik alamat URL dan User & Password. Sehingga pengguna/user bisa ganti-ganti pakai laptop dan atau komputer tablet yang manapun." +
                                "\n\nERP ekossystem menggunakan teknologi terkini, multi-platform, dilengkapi dengan ONLINE APPROVAL, dan mudah diintegrasikan dengan system lain. Group Perusahaan (induk & anak perusahaan) cukup pakai satu sistem ini, memudahkan Otomatisasi Laporan KONSOLIDASI dan terjadi efesiensi dari segi infrastruktur dan SDM (satu user bisa menangani beberapa perusahaan)."),

                        TextSpan(
                            text: "",
                            style: bodyTextStyle.copyWith(
                                fontStyle: FontStyle.italic)),
                        const TextSpan(text: ""),
                        // const TextSpan(text: "\n\n"),
                        // TextSpan(
                        //     recognizer: TapGestureRecognizer()
                        //       ..onTap = () {
                        //         openUrl(
                        //             "https://flutter.dev/docs/development/tools/hot-reload");
                        //       },
                        //     text: "Learn more",
                        //     style: bodyLinkTextStyle)
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
//----------------------------------------------------------------

//----------------------------------------------------------------
class IntroAboutUs extends StatefulWidget {
  const IntroAboutUs({Key? key}) : super(key: key);

  @override
  _IntroAboutUsState createState() => _IntroAboutUsState();
}

class _IntroAboutUsState extends State<IntroAboutUs> {
  late VideoPlayerController videoController;
  late Future<void> initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    videoController = VideoPlayerController.asset("assets/videos/FastDev.mp4");
    videoController.setVolume(0);
    videoController.setLooping(true);
    initializeVideoPlayerFuture = videoController.initialize().then((_) {
      if (mounted) {
        setState(() {});
        videoPlay();
      }
    });
  }

  @override
  void dispose() {
    videoController.dispose();
    super.dispose();
  }

  void videoPlay() {
    videoController.play();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 2,
            child: FutureBuilder(
              future: initializeVideoPlayerFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return AspectRatio(
                    aspectRatio: videoController.value.aspectRatio,
                    child: RepaintBoundary(
                        child:
                            Image.asset("assets/images/esa_cloud_intro.png")),
                  );
                } else {
                  return Container();
                }
              },
            ),
          ),
          ResponsiveRowColumnItem(
            rowFlex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 32, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(bottom: 32),
                    child: Text("ABOUT US", style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text: "ERP EkosSystem Ini adalah software aplikasi berbasis cloud,  diakses via internet, bisa dari lokasi mana pun dan komputer apapun (IOS, Windows, Android), tidak perlu Instalasi, cukup buka Internet browser (seperti Goggle CHROME, Mozilla FIREFOX) kemudian ketik alamat URL dan User & Password. Sehingga pengguna/user bisa ganti-ganti pakai laptop dan atau komputer tablet yang manapun." +
                                "\n\nERP ekossystem menggunakan teknologi terkini, multi-platform, dilengkapi dengan ONLINE APPROVAL, dan mudah diintegrasikan dengan system lain. Group Perusahaan (induk & anak perusahaan) cukup pakai satu sistem ini, memudahkan Otomatisasi Laporan KONSOLIDASI dan terjadi efesiensi dari segi infrastruktur dan SDM (satu user bisa menangani beberapa perusahaan)."),
                        TextSpan(
                            text: "",
                            style: bodyTextStyle.copyWith(
                                fontStyle: FontStyle.italic)),
                        const TextSpan(text: ""),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
//----------------------------------------------------------------

class FastDevelopment extends StatefulWidget {
  const FastDevelopment({Key? key}) : super(key: key);

  @override
  _FastDevelopmentState createState() => _FastDevelopmentState();
}

class _FastDevelopmentState extends State<FastDevelopment> {
  late VideoPlayerController videoController;
  late Future<void> initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    videoController = VideoPlayerController.asset("assets/videos/FastDev.mp4");
    videoController.setVolume(0);
    videoController.setLooping(true);
    initializeVideoPlayerFuture = videoController.initialize().then((_) {
      if (mounted) {
        // Display the first frame of the video before playback.
        setState(() {});
        videoPlay();
      }
    });
  }

  @override
  void dispose() {
    videoController.dispose();
    super.dispose();
  }

  void videoPlay() {
    videoController.play();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // ResponsiveRowColumnItem(
          //   rowFlex: 2,
          //   child: FutureBuilder(
          //     future: initializeVideoPlayerFuture,
          //     builder: (context, snapshot) {
          //       if (snapshot.connectionState == ConnectionState.done) {
          //         // If the VideoPlayerController has finished initialization, use
          //         // the data it provides to limit the aspect ratio of the VideoPlayer.
          //         return AspectRatio(
          //           aspectRatio: videoController.value.aspectRatio,
          //           // child: RepaintBoundary(child: VideoPlayer(videoController)),
          //           child: RepaintBoundary(child: Image.asset("assets/images/esa_cloud_2.png")),
          //           // child: Image.asset("assets/images/img_modul_accounting.png",
          //           //     width: 40, height: 10, fit: BoxFit.fill),
          //         );
          //       } else {
          //         // If the VideoPlayerController is still initializing, show a
          //         // loading spinner.
          //         return Container();
          //       }
          //     },
          //   ),
          // ),

          ResponsiveRowColumnItem(
            rowFlex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 130, 25, 0),
              child: ImageSlideshow(
                /// Width of the [ImageSlideshow].
                // width: 1260,
                // width: 600,
                width: ResponsiveWrapper.of(context).isMobile ? 450 : 630,

                /// Height of the [ImageSlideshow].
                // height: 600,
                // height: 400,
                height: ResponsiveWrapper.of(context).isMobile ? 200 : 300,

                /// The page to show when first creating the [ImageSlideshow].
                initialPage: 0,

                /// The color to paint the indicator.
                indicatorColor: Colors.blue,

                /// The color to paint behind th indicator.
                indicatorBackgroundColor: Colors.grey,

                /// The widgets to display in the [ImageSlideshow].
                /// Add the sample image file into the images folder
                /// Image.asset("assets/images/slide_1-bg.jpg", fit: BoxFit.fill),
                children: [
                  // Image.asset(
                  //   'assets/images/img_esa_07.jpg',
                  //   fit: BoxFit.cover,
                  // ),
                  Image.asset(
                    'assets/images/img_esa_01.jpg',
                    fit: BoxFit.cover,
                  ),
                  Image.asset(
                    'assets/images/img_esa_02.jpg',
                    fit: BoxFit.cover,
                  ),
                  Image.asset(
                    'assets/images/img_esa_03.jpg',
                    fit: BoxFit.cover,
                  ),
                  Image.asset(
                    'assets/images/img_esa_04.jpg',
                    fit: BoxFit.cover,
                  ),
                  Image.asset(
                    'assets/images/img_esa_05.jpg',
                    fit: BoxFit.cover,
                  ),
                  Image.asset(
                    'assets/images/img_esa_06.jpg',
                    fit: BoxFit.cover,
                  ),
                ],

                /// Called whenever the page in the center of the viewport changes.
                onPageChanged: (value) {
                  print('Page changed: $value');
                },

                /// Auto scroll interval.
                /// Do not auto scroll with null or 0.
                autoPlayInterval: 5000,

                /// Loops back to first slide.
                isLoop: true,
              ),
            ),
          ),

          ResponsiveRowColumnItem(
            rowFlex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 32, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // Padding(
                  //   padding: const EdgeInsets.only(bottom: 16),
                  //   // child: buildMaterialIconCircle(
                  //   //     "assets/images/icon_development.png", 68),
                  //   child: buildMaterialIconCircle(
                  //       "assets/images/img_modul_accounting.png", 68),
                  // ),
                  const Padding(
                    padding: EdgeInsets.only(bottom: 32),
                    child:
                        Text("Ringkasan (Overview)", style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text: "ERP Ekossystem adalah produk unggulan dari PT. Ekos Sistem Aplikasi (Ekossystem) yaitu perusahaan pengembang software aplikasi yang berkedudukan di Jakarta Selatan - Indonesia." +
                                "\n\nEkossystem ERP adalah Software System ERP yang  mengintegrasikan sebagian besar proses/fungsi bisnis perusahaan, melibatkan berbagai unit fungsional/departement di dalam perusahaan. dan merupakan sebuah sistem yang sesuai dengan standard Akuntansi (PSAK) yang berlaku di Indonesia." +
                                "\n\nPT. Ekos Sistem Aplikasi (Ekossystem) juga membuat modul software baru sesuai pesanan atau melakukan kustomisasi untuk klien." +
                                "\n\nPT. Ekos Sistem Aplikasi (Ekossystem) memberikan pilihan solusi sistem ERP yang tepat guna dengan kualitas  dan harga bersaing."),

                        TextSpan(
                            text: "",
                            style: bodyTextStyle.copyWith(
                                fontStyle: FontStyle.italic)),
                        const TextSpan(text: ""),
                        // const TextSpan(text: "\n\n"),
                        // TextSpan(
                        //     recognizer: TapGestureRecognizer()
                        //       ..onTap = () {
                        //         openUrl(
                        //             "https://flutter.dev/docs/development/tools/hot-reload");
                        //       },
                        //     text: "Learn more",
                        //     style: bodyLinkTextStyle)
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

//----------------------------------------------------------------

class FastDevelopmentAboutUs extends StatefulWidget {
  const FastDevelopmentAboutUs({Key? key}) : super(key: key);

  @override
  _FastDevelopmentAboutUsState createState() => _FastDevelopmentAboutUsState();
}

class _FastDevelopmentAboutUsState extends State<FastDevelopmentAboutUs> {
  late VideoPlayerController videoController;
  late Future<void> initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    videoController = VideoPlayerController.asset("assets/videos/FastDev.mp4");
    videoController.setVolume(0);
    videoController.setLooping(true);
    initializeVideoPlayerFuture = videoController.initialize().then((_) {
      if (mounted) {
        setState(() {});
        videoPlay();
      }
    });
  }

  @override
  void dispose() {
    videoController.dispose();
    super.dispose();
  }

  void videoPlay() {
    videoController.play();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // ResponsiveRowColumnItem(
          //   rowFlex: 2,
          //   child: Padding(
          //     padding: const EdgeInsets.fromLTRB(25, 130, 25, 0),
          //     child: ImageSlideshow(
          //         width: ResponsiveWrapper.of(context).isMobile ? 450 : 630,
          //         height: ResponsiveWrapper.of(context).isMobile ? 200 : 300,
          //         initialPage: 0,
          //         indicatorColor: Colors.blue,
          //         indicatorBackgroundColor: Colors.grey,
          //         children: [
          //           Image.asset(
          //             'assets/images/img_esa_01.jpg',
          //             fit: BoxFit.cover,
          //           ),
          //           Image.asset(
          //             'assets/images/img_esa_02.jpg',
          //             fit: BoxFit.cover,
          //           ),
          //           Image.asset(
          //             'assets/images/img_esa_03.jpg',
          //             fit: BoxFit.cover,
          //           ),
          //           Image.asset(
          //             'assets/images/img_esa_04.jpg',
          //             fit: BoxFit.cover,
          //           ),
          //           Image.asset(
          //             'assets/images/img_esa_05.jpg',
          //             fit: BoxFit.cover,
          //           ),
          //           Image.asset(
          //             'assets/images/img_esa_06.jpg',
          //             fit: BoxFit.cover,
          //           ),
          //         ],

          //         onPageChanged: (value) {
          //           print('Page changed: $value');
          //         },

          //         autoPlayInterval: 5000,
          //         isLoop: true,
          //   ),
          //   ),
          // ),

          ResponsiveRowColumnItem(
            rowFlex: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 32, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // const Padding(
                  //   padding: EdgeInsets.only(bottom: 32),
                  //   child: Text("Ringkasan (Overview)", style: headlineTextStyle),
                  // ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text: "ERP Ekossystem adalah produk unggulan dari PT. Ekos Sistem Aplikasi (Ekossystem) yaitu perusahaan pengembang software aplikasi yang berkedudukan di Jakarta Selatan - Indonesia." +
                                "\n\nEkossystem ERP adalah Software System ERP yang  mengintegrasikan sebagian besar proses/fungsi bisnis perusahaan, melibatkan berbagai unit fungsional/departement di dalam perusahaan. dan merupakan sebuah sistem yang sesuai dengan standard Akuntansi (PSAK) yang berlaku di Indonesia." +
                                "\n\nPT. Ekos Sistem Aplikasi (Ekossystem) juga membuat modul software baru sesuai pesanan atau melakukan kustomisasi untuk klien." +
                                "\n\nPT. Ekos Sistem Aplikasi (Ekossystem) memberikan pilihan solusi sistem ERP yang tepat guna dengan kualitas  dan harga bersaing."),
                        TextSpan(
                            text: "",
                            style: bodyTextStyle.copyWith(
                                fontStyle: FontStyle.italic)),
                        const TextSpan(text: ""),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BeautifulUI extends StatefulWidget {
  const BeautifulUI({Key? key}) : super(key: key);

  @override
  _BeautifulUIState createState() => _BeautifulUIState();
}

class _BeautifulUIState extends State<BeautifulUI> {
  late VideoPlayerController videoController;
  late Future<void> initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    videoController =
        VideoPlayerController.asset("assets/videos/FeatureMultiCompany.mp4");
    videoController.setVolume(0);
    videoController.setLooping(true);
    initializeVideoPlayerFuture = videoController.initialize().then((_) {
      if (mounted) {
        // Display the first frame of the video before playback.
        setState(() {});
        videoPlay();
      }
    });
  }

  @override
  void dispose() {
    videoController.dispose();
    super.dispose();
  }

  void videoPlay() {
    videoController.play();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 1,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 32, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // Padding(
                  //   padding: const EdgeInsets.only(bottom: 16),
                  //   child: buildMaterialIconCircle(
                  //       "assets/images/icon_ui.png", 68),
                  // ),
                  const Padding(
                    padding: EdgeInsets.only(bottom: 32),
                    child: Text("Multi Company", style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text: "ERP EkosSystem dirancang untuk digunakan oleh banyak perusahaan(Multi Company)." +
                                "\n\nGroup Perusahaan (holding, super holding dan subsidiary company) cukup pakai satu sistem ini, Otomatisasi Laporan KONSOLIDASI menjadi lebih mudah dan terjadi efesiensi dari segi infrastruktur dan SDM (satu user bisa menangani beberapa perusahaan)."),
                        // const TextSpan(text: "\n\n"),
                        // TextSpan(
                        //     recognizer: TapGestureRecognizer()
                        //       ..onTap = () {
                        //         openUrl(
                        //             "https://flutter.dev/docs/development/ui/widgets/catalog");
                        //       },
                        //     text: "Browse the widget catalog",
                        //     style: bodyLinkTextStyle),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          ResponsiveRowColumnItem(
            rowFlex: 2,
            columnOrder: 1,
            child: FutureBuilder(
              future: initializeVideoPlayerFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  // If the VideoPlayerController has finished initialization, use
                  // the data it provides to limit the aspect ratio of the VideoPlayer.
                  return AspectRatio(
                    aspectRatio: videoController.value.aspectRatio,
                    child: RepaintBoundary(child: VideoPlayer(videoController)),
                    // child: RepaintBoundary(child: Image.asset("assets/images/img_modul_finance.png")),
                  );
                } else {
                  // If the VideoPlayerController is still initializing, show a
                  // loading spinner.
                  return Container();
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}

class NativePerformance extends StatefulWidget {
  const NativePerformance({Key? key}) : super(key: key);

  @override
  _NativePerformanceState createState() => _NativePerformanceState();
}

class _NativePerformanceState extends State<NativePerformance> {
  late VideoPlayerController videoController;
  late Future<void> initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    videoController =
        VideoPlayerController.asset("assets/videos/NativePerformance.mp4");
    videoController.setVolume(0);
    videoController.setLooping(true);
    initializeVideoPlayerFuture = videoController.initialize().then((_) {
      if (mounted) {
        // Display the first frame of the video before playback.
        setState(() {});
        videoPlay();
      }
    });
  }

  @override
  void dispose() {
    videoController.dispose();
    super.dispose();
  }

  void videoPlay() {
    videoController.play();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 2,
            child: FutureBuilder(
              future: initializeVideoPlayerFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  // If the VideoPlayerController has finished initialization, use
                  // the data it provides to limit the aspect ratio of the VideoPlayer.
                  return AspectRatio(
                    aspectRatio: videoController.value.aspectRatio,
                    // child: RepaintBoundary(child: VideoPlayer(videoController)),
                    child: RepaintBoundary(
                        child:
                            Image.asset("assets/images/esa_opt_feature.jpg")),
                  );
                } else {
                  // If the VideoPlayerController is still initializing, show a
                  // loading spinner.
                  return Container();
                }
              },
            ),
          ),
          ResponsiveRowColumnItem(
            rowFlex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 32, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // Padding(
                  //   padding: const EdgeInsets.only(bottom: 16),
                  //   child: buildMaterialIconCircle(
                  //       "assets/images/icon_performance.png", 68),
                  // ),
                  const Padding(
                    padding: EdgeInsets.only(bottom: 32),
                    child: Text("ERP Modules", style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text: "Modul Utama Software ERP Ekossystem (sistem online/cloud) untuk Perusahaan adalah sebagai berikut:" +
                                "\n\n1. Accounting (GL)" +
                                "\n2. Finance/Treasury" +
                                "\n3. AR (Account Receivable)" +
                                "\n4. AP (Account Payable)" +
                                "\n5. Cash Advance Management" +
                                "\n6. FA (Fixed Asset)" +
                                "\n7. Logistic / Inventory" +
                                "\n8. Procurement" +
                                "\n9. Budget Control" +
                                "\n10. Tax Admin" +
                                "\n11. Sales" +
                                "\n\nOptional Feature tidak disertakan dalam paket, disertakan hanya jika dibutuhkan saja.\n"),

                        const TextSpan(
                            text: "", style: headlineSecondaryTextStyle),
                        TextSpan(
                            recognizer: TapGestureRecognizer()
                              //   ..onTap = () {
                              // openUrl("#");

                              // onTap: () => openUrl("/#"),

                              // onTap: () {
                              //   gotoReadmore(context);
                              // },

                              ..onTap = () {
                                gotoReadmore(context);
                              },

                            // ..onTap = () {
                            //   gotoReadmore(context);
                            //},
                            text: "\nRead More..",
                            // style: headlineSecondaryTextStyle.copyWith(
                            //     color: primary, fontSize: 18, fontStyle: FontStyle.italic)),
                            style: headlineSecondaryTextStyle.copyWith(
                                color: primary, fontSize: 18)),
                        // const TextSpan(text: "\n\n"),
                        // TextSpan(
                        //     recognizer: TapGestureRecognizer()
                        //       ..onTap = () {
                        //         openUrl("https://flutter.dev/showcase");
                        //       },
                        //     text: "Examples of apps built with Flutter",
                        //     style: bodyLinkTextStyle),
                        // const TextSpan(text: "\n\n"),
                        // TextSpan(
                        //     text: "Demo design inspired by ",
                        //     style: bodyTextStyle.copyWith(fontSize: 12)),
                        // TextSpan(
                        //     recognizer: TapGestureRecognizer()
                        //       ..onTap = () {
                        //         openUrl("https://dribbble.com/aureliensalomon");
                        //       },
                        //     text: "Aurélien Salomon",
                        //     style: bodyTextStyle.copyWith(
                        //         fontSize: 12, color: primary)),
                        // TextSpan(
                        //     text: "'s ",
                        //     style: bodyTextStyle.copyWith(fontSize: 12)),
                        // TextSpan(
                        //     recognizer: TapGestureRecognizer()
                        //       ..onTap = () {
                        //         openUrl(
                        //             "https://dribbble.com/shots/2940231-Google-Newsstand-Navigation-Pattern");
                        //       },
                        //     text: "Google Newsstand Navigation Pattern",
                        //     style: bodyTextStyle.copyWith(
                        //         fontSize: 12, color: primary)),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

void gotoReadmore(BuildContext context) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Readmore()),
  );
}

class BeautifulUI2 extends StatefulWidget {
  const BeautifulUI2({Key? key}) : super(key: key);

  @override
  _BeautifulUI2State createState() => _BeautifulUI2State();
}

class _BeautifulUI2State extends State<BeautifulUI2> {
  late VideoPlayerController videoController;
  late Future<void> initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    videoController =
        VideoPlayerController.asset("assets/videos/BeautifulUI.mp4");
    videoController.setVolume(0);
    videoController.setLooping(true);
    initializeVideoPlayerFuture = videoController.initialize().then((_) {
      if (mounted) {
        // Display the first frame of the video before playback.
        setState(() {});
        videoPlay();
      }
    });
  }

  @override
  void dispose() {
    videoController.dispose();
    super.dispose();
  }

  void videoPlay() {
    videoController.play();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 1,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 32, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // Padding(
                  //   padding: const EdgeInsets.only(bottom: 16),
                  //   child: buildMaterialIconCircle(
                  //       "assets/images/icon_ui.png", 68),
                  // ),
                  const Padding(
                    padding: EdgeInsets.only(bottom: 32),
                    //   child: Text("5. Cloud Storage",
                    //       style: headlineTextStyle),
                    // ),
                    child:
                        Text("Consolidation Modul", style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        // const TextSpan(
                        //     text:
                        //         "NASKAH disimpan dalam Cloud Storage (MICROSOFT AZURE STORAGE)."
                        //         +"\n\nSelain untuk Naskah cloud storage juga dimanfaatkan untuk Fasilitas Backup database yang dilakukan otomatis setiap hari. Data disimpan pada server terpisah (Microsoft Azure Storage) dan yang berlokasi di Singapore."
                        //         +"\n\nMicrosoft AZURE STORAGE ini adalah fasilitas cloud storage BERBAYAR dengan KEAMANAN yang bisa diandalkan."),
                        const TextSpan(
                            text: "Otomatisasi Pembuatan Laporan Keuangan Konsolidasi:" +
                                "\n\n1. Bisa dilakukan dengan COA seragam maupun tidak seragam menggunakan maping." +
                                "\n\n2. Implementasi lebih cepat karena bisa langsung dilakukan tidak perlu menunggu ganti tahun untuk penyeragaman COA." +
                                "\n\n3. Unit Bisnis (Subsidiary Company) bisa menggunakan sistem sejenis maupun sistem yang berbeda." +
                                "\n\n4. Jika Unit Bisnis menggunakan sistem berbeda, maka tersedia menu "),
                        TextSpan(
                            style: TextStyle(fontWeight: FontWeight.bold),
                            text: 'Import/Upload Excel file'),
                        TextSpan(
                            text:
                                '.\n\n5. Konsolidasi bisa untuk HOLDING dan SUPER HOLDING.'),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          ResponsiveRowColumnItem(
            rowFlex: 2,
            columnOrder: 1,
            child: FutureBuilder(
              future: initializeVideoPlayerFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  // If the VideoPlayerController has finished initialization, use
                  // the data it provides to limit the aspect ratio of the VideoPlayer.
                  return AspectRatio(
                    aspectRatio: videoController.value.aspectRatio,
                    // child: RepaintBoundary(child: VideoPlayer(videoController)),
                    // child: RepaintBoundary(child: Image.asset("assets/images/img_azure.jpg")),
                    child: RepaintBoundary(
                        child: Image.asset(
                            "assets/images/esa_consolidation2.jpg")),
                  );
                } else {
                  // If the VideoPlayerController is still initializing, show a
                  // loading spinner.
                  return Container();
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}

class FastDevelopment2 extends StatefulWidget {
  const FastDevelopment2({Key? key}) : super(key: key);

  @override
  _FastDevelopment2State createState() => _FastDevelopment2State();
}

class _FastDevelopment2State extends State<FastDevelopment2> {
  late VideoPlayerController videoController;
  late Future<void> initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    videoController = VideoPlayerController.asset("assets/videos/FastDev.mp4");
    videoController.setVolume(0);
    videoController.setLooping(true);
    initializeVideoPlayerFuture = videoController.initialize().then((_) {
      if (mounted) {
        // Display the first frame of the video before playback.
        setState(() {});
        videoPlay();
      }
    });
  }

  @override
  void dispose() {
    videoController.dispose();
    super.dispose();
  }

  void videoPlay() {
    videoController.play();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 2,
            child: FutureBuilder(
              future: initializeVideoPlayerFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  // If the VideoPlayerController has finished initialization, use
                  // the data it provides to limit the aspect ratio of the VideoPlayer.
                  return AspectRatio(
                    aspectRatio: videoController.value.aspectRatio,
                    // child: RepaintBoundary(child: VideoPlayer(videoController)),
                    // child: RepaintBoundary(child: Image.asset("assets/images/esa_modul.jpg")),
                    // child: RepaintBoundary(child: Image.asset("assets/images/esa_consolidation.jpg")),
                    child: RepaintBoundary(
                        child: Image.asset("assets/images/img_azure.jpg")),
                    // child: Image.asset("assets/images/img_modul_accounting.png",
                    //     width: 40, height: 10, fit: BoxFit.fill),
                  );
                } else {
                  // If the VideoPlayerController is still initializing, show a
                  // loading spinner.
                  return Container();
                }
              },
            ),
          ),
          ResponsiveRowColumnItem(
            rowFlex: 1,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 32, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // Padding(
                  //   padding: const EdgeInsets.only(bottom: 16),
                  //   // child: buildMaterialIconCircle(
                  //   //     "assets/images/icon_development.png", 68),
                  //   child: buildMaterialIconCircle(
                  //       "assets/images/img_modul_accounting.png", 68),
                  // ),
                  const Padding(
                    padding: EdgeInsets.only(bottom: 32),
                    // child: Text("4. Ruang Lingkup Sistem", style: headlineTextStyle),
                    // child: Text("4. Consolidation Modul", style: headlineTextStyle),
                    child:
                        Text("Azure Cloud Storage", style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        // const TextSpan(text: "Otomatisasi pembuatan Laporan keuangan Konsolidasi:"
                        // +"\n\nBisa dilakukan dengan COA seragam maupun tidak seragam menggunakan maping,"
                        // +"\nimplemntasi lebih cepat karena bisa langsung dilakukan tidak perlu menunggu ganti tahun untuk penyeragaman COA."
                        // +"\n\nUnit Bisnis (subsidiary company) bisa menggunakan sistem sejenis maupun sistem yg berbeda"
                        // +"\nJika Unit Bisnis menggunakan sistem berbeda, maka tersedia menu Import Trial Balance."
                        // +"\n\nKonsolidasi bisa untuk HOLDING dan SUPER HOLDING."),
                        const TextSpan(
                            text: "AZURE CLOUD STORAGE  adalah fasilitas cloud storage BERBAYAR dari MICROSOFT dengan KEAMANAN yang bisa diandalkan." +
                                "\n\nERP EkosSystem memanfaat Cloud Storage untuk :" +
                                "\n\n1. Fasilitas Backup , mulai dari tingkat minimal/cukup sampai dengan tingkat paranoid. (Backup database yang dilakukan otomatis setiap hari atau setiap jam). Data disimpan pada server terpisah (Microsoft Azure Storage) dan Lokasi terpisah yaitu berlokasi di Singapore." +
                                "\n\n2. Untuk menyimpan Blob  File, seperti : Naskah buku, image dan video, dan sebagainya."),
                        TextSpan(
                            text: "",
                            style: bodyTextStyle.copyWith(
                                fontStyle: FontStyle.italic)),
                        const TextSpan(text: ""),
                        // const TextSpan(text: "\n\n"),
                        // TextSpan(
                        //     recognizer: TapGestureRecognizer()
                        //       ..onTap = () {
                        //         openUrl(
                        //             "https://flutter.dev/docs/development/tools/hot-reload");
                        //       },
                        //     text: "Learn more",
                        //     style: bodyLinkTextStyle)
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class LearnFromDevelopers extends StatefulWidget {
  const LearnFromDevelopers({Key? key}) : super(key: key);

  @override
  _LearnFromDevelopersState createState() => _LearnFromDevelopersState();
}

class _LearnFromDevelopersState extends State<LearnFromDevelopers> {
  final String videoUrl = "https://www.youtube.com/embed/W1pNjxmNHNQ";
  final double videoAspectRatio = 1.78;
  UniqueKey webViewKey = UniqueKey();

  @override
  void initState() {
    super.initState();
    webViewKey = UniqueKey();
    // TODO: Breaks mobile builds. Official Flutter WebView plugin is working on Web support.
    // TODO: Resets iframe on scroll. Wait for official Flutter fix.
    // ignore: undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory(
        webViewKey.toString(),
        (viewId) => html.IFrameElement()
          ..width = "1080"
          ..height = "606"
          ..src = videoUrl
          ..style.border = "none");
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 1,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 32, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(bottom: 32),
                    child:
                        Text("Learn from developers", style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text:
                                "Watch these videos to learn from Google and developers as you build with Flutter."),
                        const TextSpan(text: "\n\n"),
                        TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                openUrl("https://www.youtube.com/flutterdev");
                              },
                            text: "Visit our YouTube playlist",
                            style: bodyLinkTextStyle),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          ResponsiveRowColumnItem(
            rowFlex: 2,
            columnOrder: 1,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: AspectRatio(
                aspectRatio: videoAspectRatio,
                child: (kIsWeb)
                    ? Image.asset(
                        "assets/images/video_thumbnail_learn_from_developers.png",
                        fit: BoxFit.contain)
                    // TODO: Disable multiple embedded iframes to prevent flicker.
//                HtmlElementView(
//                        key: webViewKey,
//                        viewType: webViewKey.toString(),
//                      )
                    : WebView(
                        key: webViewKey,
                        initialUrl: videoUrl,
                      ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class WhoUsesFlutter extends StatelessWidget {
  const WhoUsesFlutter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 5,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 24, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Text("KLIEN KAMI", style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text: "Ekossystem telah sukses mengimplementasikan Sistem ERP berbasis Cloud tersebut pada perusahaan berikut ini:" +
                                "\n\n1. METROTV (PT. Media Televisi Indonesia): Broadcast Television" +
                                "\n2. MAGNA-TV (PT Mitra Media Digital) : Broadcast Television" +
                                "\n3. BN-TV (PT Mitra Siaran Digital) : Broadcast Television" +
                                "\n4. MEDCOM (PT.Citra Multimedia Indonesia): News Portal" +
                                "\n5. IDM (PT Citra Media Kreatifindo): Brand Activation & Creative Solution" +
                                "\n7. INTERRA (PT.HARTA DJAYA KARYA): Property (Design & Build)" +
                                "\n8. Koran REPUBLIKA" +
                                "\n9. Koran MEDIA INDONESIA" +
                                "\n10. Penerbit Buku REPUBLIKA" +
                                "\n11. PT. INDOSPLICE : Rigging and Lifting Specialist" +
                                "\n12. PT. MURNI RAYA FINANCE: Financing" +
                                "\n13. PT. INDOPLAS MAKMUR LESTARI (Indoplas Energy Group)"),
                        const TextSpan(text: "\n\n"),
                        // TextSpan(
                        //     recognizer: TapGestureRecognizer()
                        //       ..onTap = () {
                        //         openUrl("https://flutter.dev/showcase");
                        //       },
                        //     text: "See what’s being created",
                        //     style: bodyLinkTextStyle),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          ResponsiveRowColumnItem(
              rowFlex: 7,
              columnOrder: 1,
              child: Image.asset("assets/images/esa-clients-logo2.png",
                  fit: BoxFit.contain)),
        ],
      ),
    );
  }
}

class ContactUs extends StatelessWidget {
  const ContactUs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ResponsiveRowColumn(
            layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
                ? ResponsiveRowColumnType.COLUMN
                : ResponsiveRowColumnType.ROW,
            rowCrossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ResponsiveRowColumnItem(
                rowFlex: 7,
                columnOrder: 2,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(25, 24, 25, 0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const Padding(
                        padding: EdgeInsets.only(bottom: 16),
                        child: Text("CONTACT US\n", style: headlineTextStyle),
                      ),

                      Table(children: [
                        TableRow(children: [
                          Text.rich(
                            TextSpan(
                              children: <InlineSpan>[
                                TextSpan(
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    text: '\nAvian Widyasmono\n'),
                              ],
                            ),
                          ),
                          Text.rich(
                            TextSpan(
                              children: <InlineSpan>[
                                WidgetSpan(
                                    alignment: ui.PlaceholderAlignment.middle,
                                    child: Icon(
                                      Icons.whatsapp_outlined,
                                      color: Colors.green,
                                      size: 20,
                                    )),
                                TextSpan(
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        openUrl(
                                            "https://api.whatsapp.com/send?phone=62816840627");
                                      },
                                    style: TextStyle(fontSize: 16),
                                    text: '+62 816-840-627'),
                                TextSpan(
                                    style: TextStyle(fontSize: 16),
                                    text: '\nEmail: avianw@ekossystem.com\n'),
                              ],
                            ),
                          ),
                        ]),
                        TableRow(children: [
                          Text.rich(
                            TextSpan(
                              children: <InlineSpan>[
                                TextSpan(
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    text: '\nEko Purnomo\n'),
                              ],
                            ),
                          ),
                          Text.rich(
                            TextSpan(
                              children: <InlineSpan>[
                                WidgetSpan(
                                    alignment: ui.PlaceholderAlignment.middle,
                                    child: Icon(
                                      Icons.whatsapp_outlined,
                                      color: Colors.green,
                                      size: 20,
                                    )),
                                TextSpan(
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        openUrl(
                                            "https://api.whatsapp.com/send?phone=6287786993229");
                                      },
                                    style: TextStyle(fontSize: 16),
                                    text: '+62 877-8699-3229'),
                                TextSpan(
                                    style: TextStyle(fontSize: 16),
                                    text:
                                        '\nEmail: eko.purnomo@ekossystem.com\n'),
                              ],
                            ),
                          ),
                        ]),
                        TableRow(children: [
                          Text.rich(
                            TextSpan(
                              children: <InlineSpan>[
                                TextSpan(
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    text: '\nDoni Tahir\n'),
                              ],
                            ),
                          ),
                          Text.rich(
                            TextSpan(
                              children: <InlineSpan>[
                                WidgetSpan(
                                    alignment: ui.PlaceholderAlignment.middle,
                                    child: Icon(
                                      Icons.whatsapp_outlined,
                                      color: Colors.green,
                                      size: 20,
                                    )),
                                TextSpan(
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        openUrl(
                                            "https://api.whatsapp.com/send?phone=6287787774252");
                                      },
                                    style: TextStyle(fontSize: 16),
                                    text: '+62 877-8777-4252'),
                                TextSpan(
                                    style: TextStyle(fontSize: 16),
                                    text:
                                        '\nEmail: doni.tahir@ekossystem.com\n'),
                              ],
                            ),
                          ),
                        ]),
                        TableRow(children: [
                          Text.rich(
                            TextSpan(
                              children: <InlineSpan>[
                                TextSpan(
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    text: '\nBudi Arianto\n'),
                              ],
                            ),
                          ),
                          Text.rich(
                            TextSpan(
                              children: <InlineSpan>[
                                WidgetSpan(
                                    alignment: ui.PlaceholderAlignment.middle,
                                    child: Icon(
                                      Icons.whatsapp_outlined,
                                      color: Colors.green,
                                      size: 20,
                                    )),
                                TextSpan(
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        openUrl(
                                            "https://api.whatsapp.com/send?phone=6285810232337");
                                      },
                                    style: TextStyle(fontSize: 16),
                                    text: '+62 858-1023-2337'),
                                TextSpan(
                                    style: TextStyle(fontSize: 16),
                                    text:
                                        '\nEmail: budiarianto.wp@gmail.com\n'),
                              ],
                            ),
                          ),
                        ]),
                        TableRow(children: [
                          Text.rich(
                            TextSpan(
                              children: <InlineSpan>[
                                TextSpan(
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                    text: '\nSigit Setiaji\n'),
                              ],
                            ),
                          ),
                          Text.rich(
                            TextSpan(
                              children: <InlineSpan>[
                                WidgetSpan(
                                    alignment: ui.PlaceholderAlignment.middle,
                                    child: Icon(
                                      Icons.whatsapp_outlined,
                                      color: Colors.green,
                                      size: 20,
                                    )),
                                TextSpan(
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        openUrl(
                                            "https://api.whatsapp.com/send?phone=6281294388137");
                                      },
                                    style: TextStyle(fontSize: 16),
                                    text: '+62 812-9438-8137'),
                                TextSpan(
                                    style: TextStyle(fontSize: 16),
                                    text: '\nEmail: setiaji24@gmail.com\n'),
                              ],
                            ),
                          ),
                        ]),
                      ]),

                      RichText(
                        text: TextSpan(
                          style: bodyTextStyle,
                          children: [
                            const TextSpan(
                                text: "\n\n\nPT. EKOS SISTEM APLIKASI" +
                                    "\nAddress: MultiPlus lt. 2, Jl. RS Fatmawati No. 72/12 Gandaria Utara, Kebayoran Baru, Jakarta Selatan 12140"),
                            const TextSpan(text: "\n\n"),

                            // TextSpan(
                            //     recognizer: TapGestureRecognizer()
                            //       ..onTap = () {
                            //         openUrl("https://flutter.dev/showcase");
                            //       },
                            //     text: "See what’s being created",
                            //     style: bodyLinkTextStyle),
                          ],
                        ),
                      ),
                      // RichText(
                      //   text: TextSpan(
                      //     style: bodyTextStyle,
                      //     children: [
                      //       const TextSpan(
                      //           text:
                      //               // "Pertanyaan dan tanggapan mengenai penawaran ini mohon ditujukan kepada primary contact. Primary contact berikut ini, yang bertanggung jawab untuk menjawab atau meneruskan pertanyaan atau tanggapan kepada pihak-pihak terkait."
                      //               "\nAvian Widyasmono            +62 816 840 627"
                      //               +"\n\nEko Purnomo                           +62 877 8699 3229"
                      //               +"\n\nDoni Tahir                                  +62 877 8777 4252"
                      //               +"\n\nBudi Arianto                              +62 858 1023 2337"
                      //               +"\n\nSigit Setiaji                                +62 812 9438 8137"
                      //               +"\n\n\n\nPT. EKOS SISTEM APLIKASI"
                      //               +"\nAddress: MultiPlus lt. 2, Jl. RS Fatmawati No. 72/12 Gandaria Utara, Kebayoran Baru, Jakarta Selatan 12140"),
                      //       const TextSpan(text: "\n\n"),

                      //       // TextSpan(
                      //       //     recognizer: TapGestureRecognizer()
                      //       //       ..onTap = () {
                      //       //         openUrl("https://flutter.dev/showcase");
                      //       //       },
                      //       //     text: "See what’s being created",
                      //       //     style: bodyLinkTextStyle),
                      //     ],
                      //   ),
                      // )
                    ],
                  ),
                ),
              ),
              ResponsiveRowColumnItem(
                rowFlex: 6,
                columnOrder: 1,
                child: AspectRatio(
                  aspectRatio: 16 / 9, //aspect ratio for Image
                  child: Image.asset(
                    "assets/images/contact_us.jpg",
                    fit: BoxFit.fill, //fill type of image inside aspectRatio
                  ),
                ),
              ),
              // fit: BoxFit.contain)
            ],
          ),
          ResponsiveRowColumn(
            layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
                ? ResponsiveRowColumnType.COLUMN
                : ResponsiveRowColumnType.ROW,
            rowCrossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ResponsiveRowColumnItem(
                rowFlex: 7,
                columnOrder: 2,
                child: maps(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class DetailModul extends StatelessWidget {
  const DetailModul({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 5,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 24, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Text(
                        "DETAIL MODUL & FEATURE\n\n" +
                            "01. Module Accounting / General Ledger",
                        style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text:
                                "Terdiri dari menu dan submenu berikut ini :"),
                        TextSpan(
                            text: "\n\nMaster Data",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Chart of Account" +
                                "\n\u2001\u2705 Dimension Account" +
                                "\n\u2001\u2001\u2001\u2022 Project, Product/Service" +
                                "\n\u2001\u2001\u2001\u2022 Department" +
                                "\n\u2001\u2001\u2001\u2022 Sales Area/Brach" +
                                "\n\u2001\u2001\u2001\u2022 Detail Costing/Budget Item, etc" +
                                "\n\u2001\u2705 Template Accrue Regular Cost" +
                                "\n\u2001\u2705 Group of Account (for Special Needs)" +
                                "\n\u2001\u2705 Company Profile & System Paremeter"),
                        TextSpan(
                            text: "\n\nTransactions",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Journal Link from Other/Sub Modul" +
                                "\n\u2001\u2705 Journal Manual Entry" +
                                "\n\u2001\u2705 Accrue Regular Cost" +
                                "\n\u2001\u2705 Amortization/Recurring Scheduled Journal" +
                                "\n\u2001\u2705 Closing Period (Month End)"),
                        TextSpan(
                            text: "\n\nReports",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 General Ledger (GL)" +
                                "\n\u2001\u2705 Trial Balance (TB), on Click Drilldown to GL, JV" +
                                "\n\u2001\u2705 Balance Sheet (BS), on Click Drilldown to TB, GL, JV" +
                                "\n\u2001\u2705 Financial Positions, on Click Drilldown to TB, GL, JV" +
                                "\n\u2001\u2705 Income Statement (Profit /(Loss)), on Click Drilldown to TB, GL, JV" +
                                "\n\u2001\u2705 Income Statement per Dimension, on Click Drilldown to JV List" +
                                "\n\u2001\u2705 Income Statement Best 10 Dimension" +
                                "\n\u2001\u2705 Income Statement Analysis by Dimension: " +
                                "\n\u2001\u2001\u2001\u2022 Profit/(Loss) per Project or per Product/Service" +
                                "\n\u2001\u2001\u2001\u2022 Cost Per Department" +
                                "\n\u2001\u2001\u2001\u2022 Revenue per Product/Service vs Area" +
                                "\n\u2001\u2001\u2001\u2022 etc. (unlimited user defined)" +
                                "\n\u2001\u2705 Change in Equity" +
                                "\n\u2001\u2705 Cash Flow (Indirect)"),
                        TextSpan(
                            text: "\n\nSettings",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Layout Balance Sheet (Unlimited User Defined)" +
                                "\n\u2001\u2705 Layout Financial Position (Unlimited User Defined)" +
                                "\n\u2001\u2705 Layout Income Statement (Unlimited User Defined)" +
                                "\n\u2001\u2705 Layout Cash Flow indirect & Change in Equity (Unlimited User Defined)"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DetailModul2 extends StatelessWidget {
  const DetailModul2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 5,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 24, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Text("02. Module Finance/Treasury",
                        style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text:
                                "Terdiri dari menu dan submenu berikut ini :"),
                        TextSpan(
                            text: "\n\nMaster Data",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Cash & Bank Account" +
                                "\n\u2001\u2705 Cash Flow Account"),
                        TextSpan(
                            text: "\n\nTransaction",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Cash/Bank Transaction List" +
                                "\n\u2001\u2001\u2001\u2022 Post to Accounting" +
                                "\n\u2001\u2001\u2001\u2022 Cash/Bank Payment Journal Voucher" +
                                "\n\u2001\u2001\u2001\u2022 Cash/Bank Receipt Journal Voucher" +
                                "\n\u2001\u2705 Cash/Bank Payment Form" +
                                "\n\u2001\u2705 Cash/Bank Receipt Form" +
                                "\n\u2001\u2705 Revaluation Bank/Cash Foreign" +
                                "\n\u2001\u2001\u2001\u2022 Post to Accounting" +
                                "\n\u2001\u2001\u2001\u2022 Cash/Bank Revaluation Journal Voucher"),
                        TextSpan(
                            text: "\n\nReconcile Account",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Bank/Cash Receipt vs GL Debit" +
                                "\n\u2001\u2705 Bank/Cash Payment vs GL Credit" +
                                "\n\u2001\u2705 Reconcile Bank Statement" +
                                "\n\u2001\u2001\u2001\u2022 Accrual Basis vs Cash Basis"),
                        TextSpan(
                            text: "\n\nReports",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Bank/Cash Book" +
                                "\n\u2001\u2705 Transaction List" +
                                "\n\u2001\u2705 Transaction with Invalid CF Acc" +
                                "\n\u2001\u2705 Cash Flow General Ledger" +
                                "\n\u2001\u2705 Cash Flow Yearly" +
                                "\n\u2001\u2705 Cash Flow by Date Range" +
                                "\n\u2001\u2705 Cash Flow with Dimension (Profit Center)"),
                        TextSpan(
                            text: "\n\nSettings",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Layout Cash Flow" +
                                "\n\u2001\u2705 Group Account CF (for special need)"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DetailModul3 extends StatelessWidget {
  const DetailModul3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 5,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 24, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Text("03. Module Account Payable",
                        style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text:
                                "Terdiri dari menu dan submenu berikut ini :"),
                        TextSpan(
                            text: "\n\nMaster Data",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Payable Group" +
                                "\n\u2001\u2705 Supplier/Vendors" +
                                "\n\u2001\u2705 W/H Tax Article - Payable"),
                        TextSpan(
                            text: "\n\nTransaction",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 AP Invoices" +
                                "\n\u2001\u2001\u2001\u2022 Link with Procurement/Purchasing module" +
                                "\n\u2001\u2001\u2001\u2022 Post to Accounting" +
                                "\n\u2001\u2001\u2001\u2022 AP Invoice Journal Voucher (Expeses/FixAsset/Inventory vs Payable)" +
                                "\n\u2001\u2001\u2001\u2022 Ready to paid from Module Treasury/Finance" +
                                "\n\u2001\u2001\u2001\u2022 Link to Fixed Asset module" +
                                "\n\u2001\u2001\u2001\u2022 Link to Logistic/Inventory module" +
                                "\n\u2001\u2705 AP Adjustment" +
                                "\n\u2001\u2001\u2001\u2022 Post to Accounting" +
                                "\n\u2001\u2001\u2001\u2022 AP Adjustment Journal Voucher" +
                                "\n\u2001\u2705 DP Invoice - Settlement" +
                                "\n\u2001\u2705 Revaluation AP Foreign Currency" +
                                "\n\u2001\u2001\u2001\u2022 Post to Accounting" +
                                "\n\u2001\u2001\u2001\u2022 AP Revaluation Journal Voucher"),
                        TextSpan(
                            text:
                                "\n\nReconcile Account - Module Account Payable",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 AP Debit vs GL Debit" +
                                "\n\u2001\u2705 AP Credit vs GL Credit" +
                                "\n\u2001\u2705 DP Invoice - Credit" +
                                "\n\u2001\u2705 DP Invoice - Debit"),
                        TextSpan(
                            text: "\n\nReports",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Summary/Aging AP per Vendor/Supplier" +
                                "\n\u2001\u2705 Summary/Aging AP per GL-Account" +
                                "\n\u2001\u2705 Payable Statement (Transaction History) per Supplier/Vendor" +
                                "\n\u2001\u2705 List W/H Tax Payable (Export CSV for e-Faktur)" +
                                "\n\u2001\u2705 List VAT In (Export CSV for e-Faktur)"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DetailModul4 extends StatelessWidget {
  const DetailModul4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 5,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 24, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Text("04. Module Account Receivable",
                        style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text:
                                "Terdiri dari menu dan submenu berikut ini :"),
                        TextSpan(
                            text: "\n\nMaster Data",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Customers" +
                                "\n\u2001\u2705 Account Receivable Group" +
                                "\n\u2001\u2705 Sales Account/Group" +
                                "\n\u2001\u2705 Outstanding Status Kinds" +
                                "\n\u2001\u2705 Deduction Payment Kinds" +
                                "\n\u2001\u2705 Adjustment Type/Group" +
                                "\n\u2001\u2705 W/H Tax Article - Prepaid"),
                        TextSpan(
                            text: "\n\nTransaction",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 AR Invoices" +
                                "\n\u2001\u2001\u2001\u2022 Import from Excel Files Another System" +
                                "\n\u2001\u2001\u2001\u2022 Manual Input/Entry" +
                                "\n\u2001\u2001\u2001\u2022 Deferred/(Unearned) Invoice" +
                                "\n\u2001\u2001\u2001\u2022 Post to Accounting (AR/Sales Journal Voucher)" +
                                "\n\u2001\u2001\u2001\u2022 Collector View" +
                                "\n\u2001\u2001\u2001\u2022 Note/Remark Collection Status" +
                                "\n\u2001\u2705 AR Invoice Adjustment" +
                                "\n\u2001\u2001\u2001\u2022 Post to Accounting" +
                                "\n\u2001\u2001\u2001\u2022 AR adjustment Journal Voucher" +
                                "\n\u2001\u2705 Payment Settlement - Collector Menu" +
                                "\n\u2001\u2001\u2001\u2022 Post to Treasury/Finance (Receipt confirmation in Bank/cash Account)" +
                                "\n\u2001\u2001\u2001\u2022 AR Settlement Voucher" +
                                "\n\u2001\u2705 Revaluation AR Foreign Currency" +
                                "\n\u2001\u2001\u2001\u2022 Post to Accounting (AR Revaluation Journal Voucher)" +
                                "\n\u2001\u2705 Deferred Invoice Realization" +
                                "\n\u2001\u2001\u2001\u2022 Open/Outstanding Deferred Account" +
                                "\n\u2001\u2001\u2001\u2022 Realization Sales Invoice" +
                                "\n\u2001\u2001\u2001\u2022 Post to Accounting (Deferred Account vs Sales  Journal Voucher)" +
                                "\n\u2001\u2705 PBB/Unearned Income Settlement" +
                                "\n\u2001\u2001\u2001\u2022 Post to Accounting (AR Settlement Journal Voucher)"),
                        TextSpan(
                            text: "\n\nReconcile Account",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 AR Debit vs GL Debit" +
                                "\n\u2001\u2705 AR Credit vs GL Credit" +
                                "\n\u2001\u2705 Inv. Unearned Income -CR" +
                                "\n\u2001\u2705 Inv. Unearned Income - DB" +
                                "\n\u2001\u2705 Payment PBB/UnEarned - Credit" +
                                "\n\u2001\u2705 Payment PBB/UnEarned - Debit" +
                                "\n\u2001\u2705 Sales/Revenue"),
                        TextSpan(
                            text: "\n\nReports",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Summary/Aging AR per Customer" +
                                "\n\u2001\u2705 Summary/Aging AR per GL-Account" +
                                "\n\u2001\u2705 Summary UnEarned Inc" +
                                "\n\u2001\u2705 Summary PBB Balance" +
                                "\n\u2001\u2705 Detail Customer Transactionn" +
                                "\n\u2001\u2705 Invoice Billing" +
                                "\n\u2001\u2705 Sales/Revenue" +
                                "\n\u2001\u2705 List of PBB (Current)" +
                                "\n\u2001\u2705 List VAT Out (PPN Keluaran)" +
                                "\n\u2001\u2705 List W/H Tax Prepaid (PPh dipungut Customer)" +
                                "\n\u2001\u2705 Target Collection Progress" +
                                "\n\u2001\u2705 Collection Realization Detail"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DetailModul5 extends StatelessWidget {
  const DetailModul5({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 5,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 24, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Text("05. Module Cash Advance",
                        style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text:
                                "Terdiri dari menu dan submenu berikut ini :"),
                        TextSpan(
                            text: "\n\nMaster Data",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Cash Advance Group" +
                                "\n\u2001\u2705 Expense Reimburse Accrue Account" +
                                "\n\u2001\u2705 Employee Database"),
                        TextSpan(
                            text: "\n\nTransaction",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Cash Advance Request" +
                                "\n\u2001\u2001\u2001\u2022 Online Requisition" +
                                "\n\u2001\u2001\u2001\u2022 Online Approval" +
                                "\n\u2001\u2001\u2001\u2022 Post to Treasury/Finance (for Payment)" +
                                "\n\u2001\u2001\u2001\u2022 Special Form for Project Budgeted - Cash Advance" +
                                "\n\u2001\u2705 Cash Advance Settlement List" +
                                "\n\u2001\u2001\u2001\u2022 Post to Accounting" +
                                "\n\u2001\u2001\u2001\u2022 Cash Advance Settlement Journal Voucher" +
                                "\n\u2001\u2001\u2001\u2022 Excess Cash will receipt from Module Treasury/Finance" +
                                "\n\u2001\u2001\u2001\u2022 Excess Expenses will be paid from Module Treasury/Finance" +
                                "\n\u2001\u2705 Expense Reimburse Request" +
                                "\n\u2001\u2001\u2001\u2022 Online Approval" +
                                "\n\u2001\u2001\u2001\u2022 Post to Accounting" +
                                "\n\u2001\u2001\u2001\u2022 Accrue Expense Reimburse Journal Voucher" +
                                "\n\u2001\u2001\u2001\u2022 Ready to paid from Module Treasury/Finance" +
                                "\n\u2001\u2001\u2001\u2022 Special Form for Project Budgeted - Cash Advance"),
                        TextSpan(
                            text: "\n\nReconcile Account",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Cash Adv. Debit vs GL Debit" +
                                "\n\u2001\u2705 Cash Adv. Credit vs GL Credit"),
                        TextSpan(
                            text: "\n\nReports",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Summary/Aging Cash Advance" +
                                "\n\u2001\u2705 Cash Advance Card (Transaction History) per Employee"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DetailModul6 extends StatelessWidget {
  const DetailModul6({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 5,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 24, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Text("06. Module Fixed Asset",
                        style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text:
                                "Terdiri dari menu dan submenu berikut ini :"),
                        TextSpan(
                            text: "\n\nMaster Data",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Category/Group Fixed Asset" +
                                "\n\u2001\u2705 Location of fixed Asset"),
                        TextSpan(
                            text: "\n\nTransaction",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Fixed Assets Record" +
                                "\n\u2001\u2001\u2001\u2022 Link from Posting - Module Account Payable" +
                                "\n\u2001\u2001\u2001\u2022 Link from Cash/bank Payment - Module Finance/treasury" +
                                "\n\u2001\u2001\u2001\u2022 Link from Accrue Expense Reimbursement - Module Cash Advance" +
                                "\n\u2001\u2001\u2001\u2022 Manual Entry" +
                                "\n\u2001\u2705 Depreciation Configuration" +
                                "\n\u2001\u2001\u2001\u2022 Depreciation Tenor/Period: auto per category vs Custom Specify period" +
                                "\n\u2001\u2001\u2001\u2022 Depreciation Method: Straight Line vs Double Decline" +
                                "\n\u2001\u2001\u2001\u2022 Depreciation Process: Post Depreciation Expense to Accounting" +
                                "\n\u2001\u2705 Depreciation Adjustment" +
                                "\n\u2001\u2001\u2001\u2022 Change Depreciation Tenor/Period or Monthly Depreciation Value" +
                                "\n\u2001\u2705 Category/Group Reclass" +
                                "\n\u2001\u2001\u2001\u2022 Post to Accounting" +
                                "\n\u2001\u2001\u2001\u2022 Category Reclass Journal Voucher" +
                                "\n\u2001\u2705 User and/or Location Mutation" +
                                "\n\u2001\u2001\u2001\u2022 Fixed Assets User History" +
                                "\n\u2001\u2001\u2001\u2022 Fixed Assets Location History" +
                                "\n\u2001\u2705 Audit/Opname" +
                                "\n\u2001\u2001\u2001\u2022 Update Latest Status of Fixed Asset"),
                        TextSpan(
                            text: "\n\nReports",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Summary Of Fixed Asset" +
                                "\n\u2001\u2705 Maintenance History" +
                                "\n\u2001\u2705 Maintenance Due/Next Schedule"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DetailModul7 extends StatelessWidget {
  const DetailModul7({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 5,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 24, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Text("07. Module Procurement/Purchasing",
                        style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text:
                                "Terdiri dari menu dan submenu berikut ini :"),
                        TextSpan(
                            text: "\n\nMaster Data",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Master Item" +
                                "\n\u2001\u2001\u2001\u2022 Minimum stock, reorder quantity" +
                                "\n\u2001\u2001\u2001\u2022 Category/Group (related to Accounting)" +
                                "\n\u2001\u2001\u2001\u2022 User Classification (related to usage experience)" +
                                "\n\u2001\u2705 Item Category/Group" +
                                "\n\u2001\u2001\u2001\u2022 Head Category (ingredients, Machine)" +
                                "\n\u2001\u2001\u2001\u2022 Category (ingredients sub category, Machine sub category: Spare Part, Consumable)" +
                                "\n\u2001\u2705 Vendor/Supplier" +
                                "\n\u2001\u2001\u2001\u2022 Supplier/Vendor for each Item" +
                                "\n\u2001\u2001\u2001\u2022 Item can be supplied by Supplier/Vendor"),
                        TextSpan(
                            text: "\n\nTransaction",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Purchase Requisition" +
                                "\n\u2001\u2001\u2001\u2022 From Modul Logistic/Inventory - related to Inventory Items" +
                                "\n\u2001\u2001\u2001\u2022 From Employee/ General Admin - related to Fixed Asset/ Services Requisition" +
                                "\n\u2001\u2705 Tendering/Bidding Process" +
                                "\n\u2001\u2001\u2001\u2022 Select Purchase Request" +
                                "\n\u2001\u2001\u2001\u2022 Send Request for Quotation (RFQ) to Supplier/Vendor" +
                                "\n\u2001\u2001\u2001\u2022 Quotation Reply, Input Price Quotation to System" +
                                "\n\u2001\u2001\u2001\u2022 Calculate Recommendation Best Supplier (High Rank/Lower Price)" +
                                "\n\u2001\u2001\u2001\u2022 Select the Winner (Best Supplier), ONLINE APPROVAL" +
                                "\n\u2001\u2001\u2001\u2022 Generate Purchase Order to Best Supplier" +
                                "\n\u2001\u2705 Purchase Order (PO)" +
                                "\n\u2001\u2001\u2001\u2022 Search/Filter by Date Range, by Project, by Department, by vendor/supplier, by Status" +
                                "\n\u2001\u2001\u2001\u2022 Search PO by item name or item description" +
                                "\n\u2001\u2001\u2001\u2022 Multiple Invoicing Term Schedule (Down Payment, Term#1, Term#2..)" +
                                "\n\u2001\u2001\u2001\u2022 Condition Apply for each Term Schedule" +
                                "\n\u2001\u2001\u2001\u2022 Online Approval" +
                                "\n\u2001\u2705 Receiving Invoice from PO with Multiple Invoicing Term" +
                                "\n\u2001\u2001\u2001\u2022 Check List on Condition Applied for Invoicing Term" +
                                "\n\u2001\u2705 Receiving Goods" +
                                "\n\u2001\u2001\u2001\u2022 Goods Conformity Inspection Form" +
                                "\n\u2001\u2001\u2001\u2022 Post Accrue to Accounting" +
                                "\n\u2001\u2001\u2001\u2022 Link to Fixed Asset module" +
                                "\n\u2001\u2001\u2001\u2022 Link to Logistic/Inventory module (Inventory Debit)"),
                        TextSpan(
                            text: "\n\nReports",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text:
                                "\n\u2001\u2705 PO, Invoicing & Payment History" +
                                    "\n\u2001\u2705 Fast Moving Item" +
                                    "\n\u2001\u2705 Slow Moving Item"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DetailModul8 extends StatelessWidget {
  const DetailModul8({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 5,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 24, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Text("08. Module Logistic/Inventory",
                        style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text:
                                "Terdiri dari menu dan submenu berikut ini :"),
                        TextSpan(
                            text: "\n\nMaster Data",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text:
                                "\n\u2001\u2705 Person/Department In charge for each Inventory Group (Mapping Category vs User)" +
                                    "\n\u2001\u2705 Store/Warehouse" +
                                    "\n\u2001\u2705 Inventory Issued Group"),
                        TextSpan(
                            text: "\n\nTransaction",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Material Request" +
                                "\n\u2001\u2001\u2001\u2022 From Modul Production - related to Bill of Material" +
                                "\n\u2001\u2001\u2001\u2022 From Employee/ General Admin - related to Another Item Category" +
                                "\n\u2001\u2705 Purchase Requisition" +
                                "\n\u2001\u2001\u2001\u2022 Based on Notification Inventory in Reorder Point" +
                                "\n\u2001\u2001\u2001\u2022 Manual Entry" +
                                "\n\u2001\u2705 Inventory Receipt" +
                                "\n\u2001\u2001\u2001\u2022 Receipt Source" +
                                "\n\u2001\u2001\u2001\u2022 From Purchase Order" +
                                "\n\u2001\u2001\u2001\u2022 Link from Posted Production Result - Module Production (transfer from WIP to Finish Good)" +
                                "\n\u2001\u2001\u2001\u2022 From Stock Audit/Opname" +
                                "\n\u2001\u2001\u2001\u2022 Receiving Process" +
                                "\n\u2001\u2001\u2001\u2022 Store/Warehouse and Shelf/Rack Placement Information" +
                                "\n\u2001\u2001\u2001\u2022 Expired Date Information" +
                                "\n\u2001\u2001\u2001\u2022 Quantity Conversion from Order PackingSize to Inventory UnitSize" +
                                "\n\u2001\u2001\u2001\u2022 Goods Conformity Inspection Form (Quality Control)" +
                                "\n\u2001\u2705 Inventory Transfer" +
                                "\n\u2001\u2001\u2001\u2022 Transfer Out to another Store/Warehouse" +
                                "\n\u2001\u2001\u2001\u2022 Transfer In - Receipt Confirmation to destination Store/Warehouse" +
                                "\n\u2001\u2705 Inventory Issued" +
                                "\n\u2001\u2001\u2001\u2022 Issued Group (usage for):" +
                                "\n\u2001\u2001\u2001\u2022 Production - Material Requisition from Production" +
                                "\n\u2001\u2001\u2001\u2022 Sales Order" +
                                "\n\u2001\u2001\u2001\u2022 Office Usage" +
                                "\n\u2001\u2001\u2001\u2022 Lost/Broken/Expired" +
                                "\n\u2001\u2001\u2001\u2022 Others (user defined in master data)" +
                                "\n\u2001\u2001\u2001\u2022 Item RETURN (excess usage or request/issued mistaken)" +
                                "\n\u2001\u2001\u2001\u2022 Post to Accounting - Inventory Credit Journal Voucher (WIP vs Inventory, COGS vs Inventory)" +
                                "\n\u2001\u2705 Inventory Stock Audit/Opname"),
                        TextSpan(
                            text: "\n\nReconcile Account",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Inventory Debit vs GL Debit" +
                                "\n\u2001\u2705 Inventory Credit vs GL Credit"),
                        TextSpan(
                            text: "\n\nReports",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Inventory Balance" +
                                "\n\u2001\u2705 Inventory Card" +
                                "\n\u2001\u2705 Inventory Need to Re-Order" +
                                "\n\u2001\u2705 Inventory Expired Date" +
                                "\n\u2001\u2705 Fast Moving Item & Slow-Moving Item"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DetailModul9 extends StatelessWidget {
  const DetailModul9({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 5,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 24, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Text("09. Module Budget Control",
                        style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text:
                                "Consist of 2(two) Kind/Type of Budget Control:"),
                        TextSpan(
                            text: "\n\nPESIMISTIC BUDGET CONTROL",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text:
                                "\nThe Actual/Realization of budget is taken from general ledger (Accounting Journal)"),
                        TextSpan(
                            text: "\n\nTransaction",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Create Annual Budget per Project or per Department" +
                                "\n\u2001\u2001\u2001\u2022 Manual Entry" +
                                "\n\u2001\u2001\u2001\u2022 Copy (+ markup) From Previous Year Budget" +
                                "\n\u2001\u2001\u2001\u2022 Copy (+ markup) From Previous Year Actual/Realization" +
                                "\n\u2001\u2705 Reclass Journal Accounting/General Ledger" +
                                "\n\u2001\u2705 Accrue Cash Advance" +
                                "\n\u2001\u2705 Accrue Received Purchase Order"),
                        TextSpan(
                            text: "\n\nReports",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Progress Budget (vs Actual/Realization)" +
                                "\n\u2001\u2001\u2001\u2022 Monthly vs Year To date" +
                                "\n\u2001\u2001\u2001\u2022 Full Column" +
                                "\nThe Actual/Realization of budget is taken from general ledger data"),
                        TextSpan(
                            text: "\n\nSettings",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text:
                                "\n\u2001\u2705 Layout Budget Report (unlimited user defines layout)"),
                        TextSpan(
                            text: "\n\nOPTIMISTIC BUDGET CONTROL",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text:
                                "\nThe Realization of budget is taken from Requisition and the actual settlement"),
                        TextSpan(
                            text: "\n\nBudget Plan (Untuk Produksi)",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Master Budget per Project/ Production Batch" +
                                "\n\u2001\u2705 Internal Budget per Event/Production" +
                                "\n\u2001\u2705 Additional Master Budget" +
                                "\n\u2001\u2705 Additional Internal Budget" +
                                "\n\u2001\u2705 Budget Production Event (short term project)"),
                        TextSpan(
                            text: "\n\nBudget Execution",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Material/Service Request (Special Form for Project Budgeted)" +
                                "\n\u2001\u2705 Cash Advance Request (Special Form for Project Budgeted)" +
                                "\n\u2001\u2705 Expense Claim (Special Form for Project Budgeted)"),
                        TextSpan(
                            text: "\n\nReports",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Progress Master Budget" +
                                "\n\u2001\u2705 Progress Internal Budget"),
                        TextSpan(
                            text: "\n\nMaster Data",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(text: "\n\u2001\u2705 Budget Cost Item"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DetailModul10 extends StatelessWidget {
  const DetailModul10({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 5,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 24, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Text("10. Module Human Resource (PAYROLL)",
                        style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text:
                                "Terdiri dari menu dan submenu berikut ini :"),
                        TextSpan(
                            text: "\n\nMaster Data",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Database Employee/Crew" +
                                "\n\u2001\u2001\u2001\u2022 Personal Detail & Employment Detail" +
                                "\n\u2001\u2001\u2001\u2022 Shift & Non-Shift (daily officer)" +
                                "\n\u2001\u2001\u2001\u2022 Variance Working Time per Project, Variance Overtime Calculation" +
                                "\n\u2001\u2001\u2001\u2022 Roster / Working Schedule (Optional Modul)" +
                                "\n\u2001\u2705 Salary Deal/Agreement" +
                                "\n\u2001\u2001\u2001\u2022 Variance Allowance per Employee or Per Project" +
                                "\n\u2001\u2705 Leave Entitlement" +
                                "\n\u2001\u2705 Employee Benefit Entitlement"),
                        TextSpan(
                            text: "\n\nTransaction",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705Timesheet" +
                                "\n\u2001\u2001\u2001\u2022 Import from Ms. Excel File" +
                                "\n\u2001\u2001\u2001\u2022 Synchronize with module Roster / Working Schedule (Optional)" +
                                "\n\u2001\u2705Employee Benefit Claim" +
                                "\n\u2001\u2705Annual Leave Claim & Compensation" +
                                "\n\u2001\u2705Employee Loan" +
                                "\n\u2001\u2705Payroll Process" +
                                "\n\u2001\u2001\u2001\u2022 Email or Print Payroll Slip to PDF"),
                        TextSpan(
                            text: "\n\nMaster File/Lookup",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Salary Items (unlimited user define salary Item)" +
                                "\n\u2001\u2705 Benefit Kind (unlimited user define employee benefit)" +
                                "\n\u2001\u2705 Client/Project" +
                                "\n\u2001\u2705 Department/Job Position/ Grade"),
                        TextSpan(
                            text: "\n\nReports",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text:
                                "\n\u2001\u2705 Payment List or BANK TRANSFER" +
                                    "\n\u2001\u2705 Annual Tax Form PPh21" +
                                    "\n\u2001\u2705 Payroll Review"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DetailModul11 extends StatelessWidget {
  const DetailModul11({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ResponsiveRowColumnItem(
            rowFlex: 5,
            columnOrder: 2,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25, 24, 25, 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Text("11. Module CONSOLIDATION (GROUP OF COMPANY)",
                        style: headlineTextStyle),
                  ),
                  RichText(
                    text: TextSpan(
                      style: bodyTextStyle,
                      children: [
                        const TextSpan(
                            text:
                                "Terdiri dari menu dan submenu berikut ini :"),
                        TextSpan(
                            text: "\n\nMaster Data",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Unit Business (Consolidation Member)" +
                                "\n\u2001\u2001\u2001\u2022 Online (using EkosSystem ERP)" +
                                "\n\u2001\u2001\u2001\u2022 Offline (using other sistem , Data source Import/upload)" +
                                "\n\u2001\u2705 CONSOLIDATION Chart of Account (COA)" +
                                "\n\u2001\u2705 Maping COA Unit Bisnis"),
                        TextSpan(
                            text: "\n\nTransaction",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Trial Balance dari Unit Bisnis" +
                                "\n\u2001\u2001\u2001\u2022 Online (get from Ekossytem ERP) " +
                                "\n\u2001\u2001\u2001\u2022 Offline (using other sistem , Data source Import/upload)" +
                                "\n\u2001\u2705 Trial Balance dari Unit Bisnis" +
                                "\n\u2001\u2705 Jurnal Reklass/Eliminasi" +
                                "\n\u2001\u2705 Kertas Kerja Konsolidation"),
                        TextSpan(
                            text: "\n\nReports",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Balance Sheet" +
                                "\n\u2001\u2705 Financial Positions" +
                                "\n\u2001\u2705 Income Statement (Profit /(Loss))" +
                                "\n\u2001\u2705 Cash Flow, Change in Equity, etc"),
                        TextSpan(
                            text: "\n\nReports Setting (user defined)",
                            style: bodyTextStyle.copyWith(
                                fontWeight: FontWeight.bold)),
                        const TextSpan(
                            text: "\n\u2001\u2705 Layout Balance Sheet" +
                                "\n\u2001\u2705 Layout Financial Position" +
                                "\n\u2001\u2705 Layout Income Statement" +
                                "\n\u2001\u2705 Layout Cash Flow, Change Equity, etc"),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class FlutterNewsRow extends StatelessWidget {
  const FlutterNewsRow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: blockMargin,
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isSmallerThan(DESKTOP)
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        rowCrossAxisAlignment: CrossAxisAlignment.start,
        rowSpacing: 25,
        columnSpacing: 32,
        children: const [
          ResponsiveRowColumnItem(
            rowFlex: 1,
            rowFit: FlexFit.tight,
            child: FlutterNewsCard(
              title: "Announcing Flutter 1.12",
              imagePath: "assets/images/news_flutter_1.12.png",
              linkUrl:
                  "https://developers.googleblog.com/2019/12/flutter-ui-ambient-computing.html",
            ),
          ),
          ResponsiveRowColumnItem(
            rowFlex: 1,
            rowFit: FlexFit.tight,
            child: FlutterNewsCard(
              title: "CodePen now supports Flutter",
              imagePath: "assets/images/news_flutter_codepen.png",
              linkUrl:
                  "https://medium.com/flutter/announcing-codepen-support-for-flutter-bb346406fe50",
            ),
          ),
        ],
      ),
    );
  }
}

class FlutterNewsCard extends StatelessWidget {
  final String title;
  final String imagePath;
  final String linkUrl;

  const FlutterNewsCard(
      {Key? key,
      required this.title,
      required this.imagePath,
      required this.linkUrl})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            constraints: const BoxConstraints(maxHeight: 400),
            child: Image.asset(imagePath, fit: BoxFit.fitWidth),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(40, 40, 40, 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 16),
                  child: Text("News",
                      style: bodyTextStyle.copyWith(
                          fontSize: 12, color: const Color(0xFF6C757D))),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16),
                  child: Text(title, style: headlineSecondaryTextStyle),
                ),
                GestureDetector(
                  onTap: () => openUrl(linkUrl),
                  child: Text("Read More", style: bodyLinkTextStyle),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class FlutterCodelab extends StatefulWidget {
  const FlutterCodelab({Key? key}) : super(key: key);

  @override
  _FlutterCodelabState createState() => _FlutterCodelabState();
}

class _FlutterCodelabState extends State<FlutterCodelab>
    with AutomaticKeepAliveClientMixin<FlutterCodelab> {
  static List<String> codelabIds = ["Spinning Flutter", "Fibonacci", "Counter"];
  static List<String> codelabUrls = [
    "https://dartpad.dev/embed-flutter.html?id=c0450ca427127acfb710a31c99761f1a",
    "https://dartpad.dev/embed-flutter.html?id=38311b87e4b3c76329812077c82323b4",
    "https://dartpad.dev/embed-flutter.html?id=7b5710b344431457753253625a596158"
  ];
  String codelabSelected = codelabIds[0];
  String codelabUrlSelected = codelabUrls[0];
  final double videoAspectRatio = 1.75;

  late Map<String, Widget> codelabExamples;
  // TODO: Breaks mobile builds. Official Flutter WebView plugin is working on Web support.
  HtmlElementView? codelabHtmlElementView;
  UniqueKey webViewKey = UniqueKey();

  @override
  void initState() {
    super.initState();
    codelabExamples = <String, Widget>{
      codelabIds[0]: getCupertinoSelectionWidget(codelabIds[0]),
      codelabIds[1]: getCupertinoSelectionWidget(codelabIds[1]),
      codelabIds[2]: getCupertinoSelectionWidget(codelabIds[2]),
    };
    setCodelabHtmlElementView();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: Align(
        alignment: Alignment.center,
        child: Container(
          constraints: const BoxConstraints(maxWidth: 1000),
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.only(bottom: 16),
                child: Text("Try Flutter in your browser",
                    style: headlineTextStyle),
              ),
              CupertinoSlidingSegmentedControl(
                groupValue: codelabSelected,
                onValueChanged: (dynamic value) => setCodelabSelected(value),
                children: codelabExamples,
              ),
              RepaintBoundary(
                key: webViewKey,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(25, 16, 25, 16),
                  child: AspectRatio(
                    aspectRatio: videoAspectRatio,
                    child: (kIsWeb)
                        ? Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: const Color(0xFFD3D3D3), width: 1),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(0)),
                            ),
                            child: HtmlElementView(
                              key: webViewKey,
                              viewType: codelabSelected,
                            ),
                          )
                        : WebView(
                            key: webViewKey,
                            initialUrl: codelabUrlSelected,
                          ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 16),
                child: RichText(
                  text: TextSpan(
                    style: headlineSecondaryTextStyle,
                    children: [
                      const TextSpan(text: "Want more practice? "),
                      TextSpan(
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              openUrl("https://flutter.dev/codelabs");
                            },
                          text: "Try a codelab",
                          style: headlineSecondaryTextStyle.copyWith(
                              color: primary)),
                      const TextSpan(text: ".")
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  void setCodelabSelected(String codelab) {
    codelabSelected = codelab;
    codelabUrlSelected = codelabUrls[codelabIds.indexOf(codelab)];
    setCodelabHtmlElementView();
    setState(() {});
  }

  void setCodelabHtmlElementView() {
    // ignore: undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory(
        codelabSelected,
        (viewId) => html.IFrameElement()
          ..width = "1080"
          ..height = "617"
          ..src = codelabUrlSelected
          ..style.border = "none");
  }

  Widget getCupertinoSelectionWidget(String text) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Text(text, style: bodyTextStyle),
    );
  }
}

class InstallFlutter extends StatelessWidget {
  const InstallFlutter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: border)),
      margin: blockMargin,
      padding: blockPadding(context),
      child: Align(
        alignment: Alignment.center,
        child: Container(
          constraints: const BoxConstraints(maxWidth: 800),
          child: Column(
            children: [
              const Padding(
                padding: EdgeInsets.only(bottom: 16),
                child: Text("Install Flutter today.", style: headlineTextStyle),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 16),
                child: Text("It’s free and open source.",
                    style: bodyTextStyle.copyWith(fontSize: 24)),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 32),
                child: TextButton(
                  onPressed: () =>
                      openUrl("https://flutter.dev/docs/get-started/install"),
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(primary),
                      overlayColor: MaterialStateProperty.resolveWith<Color>(
                        (Set<MaterialState> states) {
                          if (states.contains(MaterialState.hovered)) {
                            return buttonPrimaryDark;
                          }
                          if (states.contains(MaterialState.focused) ||
                              states.contains(MaterialState.pressed)) {
                            return buttonPrimaryDarkPressed;
                          }
                          return primary;
                        },
                      ),
                      // Shape sets the border radius from default 3 to 0.
                      shape: MaterialStateProperty.resolveWith<OutlinedBorder>(
                        (Set<MaterialState> states) {
                          if (states.contains(MaterialState.focused) ||
                              states.contains(MaterialState.pressed)) {
                            return const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(0)));
                          }
                          return const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0)));
                        },
                      ),
                      padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                          const EdgeInsets.symmetric(
                              vertical: 32, horizontal: 90)),
                      // Side adds pressed highlight outline.
                      side: MaterialStateProperty.resolveWith<BorderSide>(
                          (Set<MaterialState> states) {
                        if (states.contains(MaterialState.focused) ||
                            states.contains(MaterialState.pressed)) {
                          return const BorderSide(
                              width: 3, color: buttonPrimaryPressedOutline);
                        }
                        // Transparent border placeholder as Flutter does not allow
                        // negative margins.
                        return const BorderSide(width: 3, color: Colors.white);
                      })),
                  child: Text(
                    "Get started",
                    style: buttonTextStyle.copyWith(fontSize: 18),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Footer extends StatelessWidget {
  const Footer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundDark,
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: ResponsiveRowColumn(
        layout: ResponsiveWrapper.of(context).isMobile
            ? ResponsiveRowColumnType.COLUMN
            : ResponsiveRowColumnType.ROW,
        columnMainAxisSize: MainAxisSize.min,
        children: [
          ResponsiveRowColumnItem(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 20, 20, 20),
              child: Image.asset("assets/images/esa_logo_bgblue_small.png",
                  height: 100, fit: BoxFit.contain),
            ),
          ),
          ResponsiveRowColumnItem(
            rowFit: FlexFit.loose,
            columnFit: FlexFit.loose,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    IconButton(
                        // Use the FaIcon Widget + FontAwesomeIcons class for the IconData
                        icon: FaIcon(FontAwesomeIcons.youtubeSquare,
                            size: 30, color: primaryLight),
                        onPressed: () {
                          openUrl('https://www.youtube.com/');
                        }),
                    // const SizedBox(width: 0),
                    IconButton(
                        // Use the FaIcon Widget + FontAwesomeIcons class for the IconData
                        icon: FaIcon(FontAwesomeIcons.facebookSquare,
                            size: 30, color: primaryLight),
                        onPressed: () {
                          openUrl('https://web.facebook.com/');
                        }),
                    // const SizedBox(width: 0),
                    IconButton(
                        // Use the FaIcon Widget + FontAwesomeIcons class for the IconData
                        icon: FaIcon(FontAwesomeIcons.instagram,
                            size: 30, color: primaryLight),
                        onPressed: () {
                          openUrl("https://www.instagram.com/");
                        }),
                    const SizedBox(width: 5),

                    RichText(
                      textAlign: TextAlign.left,
                      text: TextSpan(
                        style: bodyTextStyle.copyWith(
                            fontSize: 14, color: Colors.white, height: 2),
                        children: [
                          TextSpan(
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  openUrl("#");
                                },
                              text:
                                  "Copyright © 2022. All rights reserved | PT. EKOS SISTEM APLIKASI"),
                          // const TextSpan(text: "  •  "),
                          // TextSpan(
                          //     recognizer: TapGestureRecognizer()
                          //       ..onTap = () {
                          //         openUrl("https://flutter.dev/tos");
                          //       },
                          //     text: "terms"),
                          // const TextSpan(text: "  •  "),
                          // TextSpan(
                          //     recognizer: TapGestureRecognizer()
                          //       ..onTap = () {
                          //         openUrl("https://flutter.dev/security");
                          //       },
                          //     text: "security"),
                          // const TextSpan(text: "  •  "),
                          // TextSpan(
                          //     recognizer: TapGestureRecognizer()
                          //       ..onTap = () {
                          //         openUrl(
                          //             "https://www.google.com/intl/en/policies/privacy");
                          //       },
                          //     text: "privacy"),
                          // const TextSpan(text: "  •  "),
                          // TextSpan(
                          //     recognizer: TapGestureRecognizer()
                          //       ..onTap = () {
                          //         openUrl("https://flutter-es.io/");
                          //       },
                          //     text: "español"),
                          // const TextSpan(text: "  •  "),
                          // TextSpan(
                          //     recognizer: TapGestureRecognizer()
                          //       ..onTap = () {
                          //         openUrl("https://flutter.cn/");
                          //       },
                          //     text: "社区中文资源"),
                        ],
                      ),
                    ),
                  ],
                ),
                RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                      style: bodyTextStyle.copyWith(
                          fontSize: 14, color: Colors.white, height: 1),
                      children: [
                        const TextSpan(text: '\n'),
                        TextSpan(
                            text:
                                "Multiplus lt.2, Jl. RS Fatmawati No. 72/12 Gandaria Utara, Kebayoran Baru, Jakarta Selatan 12140. Telp. (021) 2222-4445 | Email: information@ekossystem.com",
                            style: bodyTextStyle.copyWith(
                                fontSize: 10, color: Colors.white)),
                        const TextSpan(text: '\n'),
                        const TextSpan(text: '\n'),
                        const TextSpan(text: '\n'),
                      ]),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
