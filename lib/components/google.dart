import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:google_maps_flutter_web/google_maps_flutter_web.dart';

class maps extends StatefulWidget {
  const maps({ Key? key }) : super(key: key);

  @override
  _mapsState createState() => _mapsState();
}

class _mapsState extends State<maps> {
  late GoogleMapController mapscontroller;
  List<Marker> _markers = [];
  bool showmaps = true;

  @override
  void initState() {
    super.initState();
    _markers.add(Marker(
      markerId: MarkerId('mylocation'),
      position: LatLng(-6.260445991589112, 106.79645350020844),
      ));
    if(_markers.isNotEmpty) {
      setState(() {
        showmaps = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: showmaps
      ? Container(
          height: 300,
          width: 1230,
          decoration: 
            BoxDecoration(borderRadius: BorderRadius.circular(12)),
          child: GoogleMap(
            onMapCreated: (controller) {
              setState(() {
                mapscontroller = controller;
              });
            },
            markers: Set<Marker>.of(_markers),
            mapType: MapType.terrain,
            initialCameraPosition: CameraPosition(
              target: LatLng(-6.260445991589112, 106.79645350020844), zoom: 13)),
      )
      //
      : CircularProgressIndicator(
        color: Colors.amber,
      ));
  }
}